/**
 * Created by keyuhan on 2020/1/26.
 */
var ASS_File = {

    uploadFileId: "",

    currentTime : 0,

    bytesPerPiece: 1024 * 1024 * 2, // 文件切片大小定义 2M

    // upload model file from pc
    upload: function (file) {

        file = !file ? ASS_UI.uploadUI.uploadInputFile : file;

        // open file dia to import file
        file.onchange = function () {

            //import
            var oFReader = new FileReader();

            oFReader.onload = function (e) {

                // alert("加载完成！");

                checkFileType(file.files[0]);

            };

            oFReader.readAsText(file.files[0]);

            ASS_File.currentTime = new Date().getTime();

        };

        file.style.display = "block";

        // check file type name and upload to web
        function checkFileType(file) {

            var name = file.name;

            var type = name.split('.')[1].toUpperCase();

            if("SKP,DWG,RVT".includes(type)){

                // ASS_File.uploadFileToWeb(file, file.name);

                ASS_File.uploadBigFile(file, file.name);
            }
        }
    },

    uploadBigFile: function (file, fileName) {

        var blob = file;

        var bytesPerPiece = ASS_File.bytesPerPiece;

        var filesize = blob.size;

        var totalPieces = Math.ceil(filesize / bytesPerPiece);

        var time= Math.floor((new Date().valueOf())/ 1000);

        var index= 1;

        var start = 0;

        var end = 0;

        ASS_AJAX.setProgressValue({

            'isShowProgress':true,

            'progressTopic':'上传文件',

            'progressBy':'loading',

            'progressSize':0,

            'totalPieces':totalPieces,

            'restart':true

        });

        while(start< filesize) {

            end = start + bytesPerPiece;

            if (end > filesize) {

                end = filesize;
            }

            //切割文件
            var chunk = blobSlice(blob, start, end);

            //传参
            var oMyForm= new FormData();

            oMyForm.append("file", chunk);

            oMyForm.append("baseFileName", fileName);

            oMyForm.append("fileName", time + fileName);

            oMyForm.append("totalPieces", totalPieces);

            oMyForm.append("nowPieces", index);

            // formData.append("md5", md5);

            var url = "php/uploadBigFile.php";

            var callback = function (response) {

                if(response != "") {

                    response = JSON.parse(response);

                    if (response['code'] == "success") {

                        console.log("上传大文件成功！" + response['userId']);

                        ASS_Cursor.showProgress(100, 'topic', '上传文件成功');

                        ASS_File.uploadFileToBimface(fileName, response['userId']);

                    }

                }
            };

            ASS_AJAX.loadObject("POST", url, oMyForm, callback);

            start = end;

            index++;

        }

        // slice blob
        function blobSlice(blob,startByte,endByte){

            if(blob.slice){
                return  blob.slice(startByte,endByte);
            }
            // 兼容firefox
            if(blob.mozSlice){
                return  blob.mozSlice(startByte,endByte);
            }
            // 兼容webkit
            if(blob.webkitSlice){
                return  blob.webkitSlice(startByte,endByte);
            }
            return null;
        }

    },

    uploadFileToBimface: function (fileName, userId) {

        var oMyForm = new FormData();

        // oMyForm.append("Authorization", ASS_AccessToken.accessToken);

        oMyForm.append("userId", userId);

        oMyForm.append("fileName", encodeURI(fileName));

        var url = "php/putModel.php";

        var callback = function (response) {

            if(response != ""){

                response = JSON.parse(response);

                if (response['code'] == "success") {

                    ASS_File.uploadFileId = response['fileId'];

                    console.log("提交转换文件成功！" + ASS_File.uploadFileId);

                    ASS_Cursor.showProgress(100, 'topic', '提交文件成功');

                    ASS_File.getTranslationOnLoop();

                } else {

                    alert("提交转换文件失败：" + response['msg']);

                }

            } else {

                alert("提交文件失败：" + response);

            }

        };

        ASS_AJAX.loadObject("POST", url, oMyForm, callback);

        ASS_AJAX.setProgressValue({

            'isShowProgress':true,

            'progressTopic':'提交文件',

            'progressBy':'time',

            'startTime':new Date().getTime(),

            'restart':true

        });

    },

    // get file translation on loop by 1s
    getTranslationOnLoop: function () {

        console.log("查询文件转换状态!");

        setTimeout(ASS_File.getFileTranslateStatus,1000);

    },

    // get file translate status
    getFileTranslateStatus: function () {

        var fileId = ASS_File.uploadFileId + "";

        if(fileId != ""){

            var oMyForm = new FormData();

            // oMyForm.append("Authorization", ASS_AccessToken.accessToken);

            oMyForm.append("fileId", fileId);

            var url = "php/getTranslating.php";

            var callback = function (response) {

                if(response != ""){

                    response = JSON.parse(response);

                    var time = (new Date().getTime() - ASS_File.currentTime) / 1000;

                    if (response['code'] == "success" && response['data']['status'] == "success") {

                        ASS_Cursor.showProgress(100, 'topic', '完成');

                        // if(window.confirm("上传和转换文件成功，打开在线文件？ 共使用 " + time.toFixed(2) + " 秒")){
                        //
                        //     window.location.href = "https://www.edraw.xyz/online/model.php?fileId=" + fileId;// open model on line
                        // }

                    } else {

                        console.log("转换文件未完成!");

                        ASS_Cursor.showProgress(Math.round(time % 100), 'topic', '转换文件');

                        ASS_File.getTranslationOnLoop();

                    }

                }

                else {

                    alert("转换文件失败：" + response);

                }
            };

            ASS_AJAX.loadObject("POST", url, oMyForm, callback);

        }
    },
};