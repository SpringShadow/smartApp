/**
 * Created by keyuhan on 2020/1/23.
 */
var ASS_ViewToken = {

    fileId: "",// 1785322525411168

    viewToken:"", // 713250a62e58408ba47fa4e7e8ff78ef

    // get view token of file by file id
    getViewToken : function (accessToken, fileId) {

        var oMyForm = new FormData();

        oMyForm.append("accessToken", accessToken);

        oMyForm.append("fileId", fileId);

        var url = "php/getViewToken.php";

        var callback = function (response) {

            if(response != ""){

                response = JSON.parse(response);

                if (response['code'] == "success") {

                    ASS_ViewToken.fileId = fileId;

                    ASS_ViewToken.viewToken = response['data'];

                    ASS_Model.show(ASS_ViewToken.viewToken);

                } else {

                    ASS_ViewToken.viewToken = "failure";

                    alert('在线文件暂时无法查看，前稍后再试！');
                }

                console.log("viewToken: " + ASS_ViewToken.viewToken);
            }
        };

        ASS_AJAX.loadObject("POST", url, oMyForm, callback);
    },


    // get view token of file by id
    getViewTokenByFileId : function (fileId) {

        // callback on find access token
        var callback = function (response) {

            if(response != ""){

                response = JSON.parse(response);

                // find access token is success
                if (response['code'] == "success") {

                    ASS_AccessToken.accessToken = response['data']['token'];

                    ASS_AccessToken.expireTime = response['data']['expireTime'];

                    console.log("AccessToken: " + ASS_AccessToken.accessToken);

                    console.log("expireTime: " + ASS_AccessToken.expireTime);

                    // find view token with file id
                    ASS_ViewToken.getViewToken(ASS_AccessToken.accessToken, fileId);
                }
            }
        };

        ASS_AccessToken.getAccessTokenByCallback(callback);
    },

};