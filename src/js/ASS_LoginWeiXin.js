/**
 * Created by keyuhan on 2020/1/27.
 */
var ASS_LoginWeiXin = {

    login:"",

    create: function (){

        ASS_UI.loginUI.create();

        this.load();
    },

    load: function () {
        var wx_login_div = document.getElementById('wx_login_div');
        if(wx_login_div.getElementsByTagName("iframe").length === 0){
            var iframe = document.createElement("iframe");
            iframe.src="https://open.weixin.qq.com/connect/qrconnect?appid=wxcb7237f3ff9cd5d1&scope=snsapi_login&redirect_uri=https%3A%2F%2Fwww.edraw.xyz%2Fonline%2Fphp%2FwxLogin.php&state=kCC0gQaJUPn1I4WwGa1iQupNNVylxIF2&login_type=jssdk&self_redirect=true&style=black&href=https://www.edraw.xyz/online/css/wx_login.css";// 微信电脑端登录
            iframe.width = "100%";
            iframe.height = "100%";
            iframe.frameborder = "0";
            iframe.scrolling = "no";
            iframe.style = "border-width:0;";
            iframe.target="_parent";
            wx_login_div.appendChild(iframe);
        }
    }

};