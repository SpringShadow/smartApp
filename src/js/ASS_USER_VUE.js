/**
 * Created by keke on 2020/1/30.
 */
var ASS_USER_VUE = {


    onStart: function () {

        new Vue({

            el: '#ass_body',

            vuetify: new Vuetify(),

            props: {
                source: String,
            },

            data: {
                dialog: false,
                drawer: null,
                items: [
                    { icon: 'mdi-contacts', text: 'Contacts' },
                    { icon: 'mdi-history', text: 'Frequently contacted' },
                    { icon: 'mdi-content-copy', text: 'Duplicates' },
                    {
                        icon: 'mdi-chevron-up',
                        'icon-alt': 'mdi-chevron-down',
                        text: 'Labels',
                        model: true,
                        children: [
                            { icon: 'mdi-plus', text: 'Create label' },
                        ],
                    },
                    {
                        icon: 'mdi-chevron-up',
                        'icon-alt': 'mdi-chevron-down',
                        text: 'More',
                        model: false,
                        children: [
                            { text: 'Import' },
                            { text: 'Export' },
                            { text: 'Print' },
                            { text: 'Undo changes' },
                            { text: 'Other contacts' },
                        ],
                    },
                    { icon: 'mdi-settings', text: 'Settings' },
                ],

                user:

                {
                    text:ASS.user.name,

                    pic:ASS.user.picture,

                },

                date: new Date().toISOString().substr(0, 10),

                dateMenu: false,

                mini: true,
    
            },
        });

    }

};