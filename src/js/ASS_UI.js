/**
 * Created by keyuhan on 2020/1/28.
 */
var ASS_UI = {

    loginUI:{

        logo_white_src: "image/logo/edraw_logo_font_white.svg",
        
        create:function () {

            // create sidebar div
            var user_background = document.createElement("div");
            var parent = document.getElementById("ass_body");
            user_background.className = "ass_sign";
            parent.appendChild(user_background);

            // create logo div
            var login = document.createElement("div");
            user_background.appendChild(login);
            var img_div = document.createElement("div");//user div
            var logo = document.createElement("img");//user
            img_div.className = "ass_sign_img";
            logo.src = ASS_UI.loginUI.logo_white_src;
            login.appendChild(img_div);
            img_div.appendChild(logo);
            logo.style.width = "40%";
            logo.style.height = "50%";
            logo.style.margin = "15% 30% 15% 30%";
            logo.style.borderRadius = "0px";
            logo.style.cursor = "default";
            img_div.style.height = "150px";

            // create weixin login div
            var weixin = document.createElement("div");
            weixin.className = "ass_sign_form";
            weixin.id = 'wx_login_div';
            weixin.style.height = "500px";
            login.appendChild(weixin);

        }
    },
    
    uploadUI:{

        uploadInputFile:'',
        
        create: function () {

            // // create sidebar div
            // var user_background = document.createElement("div");
            // var parent = document.getElementById("ass_body");
            // user_background.className = "ass_sign";
            // parent.appendChild(user_background);
            //
            // // create user name and picture div
            // var user = document.createElement("div");
            // user_background.appendChild(user);
            // var img_div = document.createElement("div");//user div
            // var userPicture = document.createElement("img");//user picture
            // var userName = document.createElement("span");//user picture
            // img_div.className = "ass_sign_img";
            // userPicture.src = ASS.user.picture;
            // userName.innerText  = ASS.user.name;
            //
            // user.appendChild(img_div);
            // img_div.appendChild(userPicture);
            // img_div.appendChild(userName);
            // userPicture.style.width = "8vh";
            // userPicture.style.height = "8vh";
            // userPicture.style.margin = "6vh 4vh";
            // userPicture.style.borderRadius = "0px";
            // userPicture.style.cursor = "default";
            // userPicture.style.borderRadius = "50%";
            // img_div.style.height = "20vh";
            //
            // var layoutTree = document.createElement("div");
            // layoutTree.className = "ass_sign_form_1";
            // layoutTree.id = 'wx_login_div';
            // layoutTree.style.height = "80vh";
            // layoutTree.style.backgroundColor = "#205081";
            // layoutTree.innerHTML =
            //     '<ul class="layui-nav layui-nav-tree layui-inline layui-bg-blue" lay-filter="demo" style="width: 40vh;">'+
            //     '<li class="layui-nav-item layui-nav-itemed">'+
            //     '<a href="javascript:;">默认展开</a>'+
            //     '<dl class="layui-nav-child">'+
            //     '<dd><a href="javascript:;">选项一</a></dd>'+
            //     '<dd><a href="javascript:;">选项二</a></dd>'+
            //     '<dd><a href="javascript:;">选项三</a></dd>'+
            //     '<dd><a href="">跳转项</a></dd>'+
            //     '</dl>'+
            //     '</li>'+
            //     '<li class="layui-nav-item">'+
            //     '<a href="javascript:;">解决方案</a>'+
            //     '<dl class="layui-nav-child">'+
            //     '<dd><a href="">移动模块</a></dd>'+
            //     '<dd><a href="">后台模版</a></dd>'+
            //     '<dd><a href="">电商平台</a></dd>'+
            //     '</dl>'+
            //     '</li>'+
            //     '<li class="layui-nav-item"><a href="">云市场</a></li>'+
            //     '<li class="layui-nav-item"><a href="">社区</a></li>'+
            //     '</ul>';
            // user.appendChild(layoutTree);

            // create file input
            // this.createInputFile();

            // ASS_UI.layui.int();
        },

        // create file input
        createInputFile: function () {

            var file = document.createElement("input");

            file.type = "file";

            document.getElementById('domId').appendChild(file);

            ASS_UI.uploadUI.uploadInputFile = file;

        }
    },


    layui: {

        int: function () {

            layui.use('element', function(){

                var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块

                //监听导航点击
                element.on('nav(demo)', function(elem){

                    console.log(elem.text());
                    //layer.msg(elem.text());

                });

            });

        }
    }

};