/**
 * Created by keke on 2020/1/30.
 */
var ASS_VUE = {

    onStart: function () {

        new Vue({

            el: '#ass_body',

            vuetify: new Vuetify(),

            data: {

                drawer: true,

                items: [

                    { title: 'Home', icon: 'mdi-home-city' },

                    { title: 'My Account', icon: 'mdi-account' },

                    { title: 'Users', icon: 'mdi-account-group-outline' },

                ],

                mini: true,

                sheet: false,

                links: [

                    'Home',

                    'Services',

                    'Blog',

                    'Contact Us',

                ],

                date: new Date().toISOString().substr(0, 10),

                dialog: false,

                dateMenu: false,

                user:

                {
                    text:ASS.user.name,

                    pic:ASS.user.picture,

                },

            },

        });

    },

    projectionCard: {

        interHtml:
            '<template>'+
                '<v-card'+
                    'class="mx-auto v-card-float-ass"'+
                    'max-width="400"'+
                    '>'+
                    '<v-img'+
                        'class="white--text align-end card-image-project-ass"'+
                        'height="200px"'+
                        'src="image/icon/project_background.png"'+
                        '>'+
                        '<v-card-title>项目名称</v-card-title>'+
                    '</v-img>'+
                    '<v-card-subtitle class="pb-0">创建时间：{{ date }}</v-card-subtitle>'+
                    '<v-card-text class="text--gray-ass">'+
                        '<div>项目地点：浙江 杭州</div>'+
                    '</v-card-text>'+
                    '<v-card-text class="text--primary">'+
                        '<div>项目成员：{{user.text}}</div>'+
                    '</v-card-text>'+

                    '<v-card-actions>'+
                        '<v-btn icon>'+
                            '<v-icon>mdi-heart</v-icon>'+
                        '</v-btn>'+

                        '<v-btn icon>'+
                            '<v-icon>mdi-delete</v-icon>'+
                        '</v-btn>'+

                        '<v-btn icon>'+
                            '<v-icon>mdi-share-variant</v-icon>'+
                        '</v-btn>'+

                        '<template>'+
                            '<v-dialog v-model="dialog" persistent max-width="600px">'+
                                '<template v-slot:activator="{ on }">'+
                                    '<v-btn v-on="on" style="margin-left: 8px"icon>'+
                                        '<v-icon>mdi-pencil</v-icon>'+
                                    '</v-btn>'+
                                '</template>'+
                                '<v-card'+
                                    'height="500px"'+
                                    '>'+
                                    '<v-card-title>'+
                                        '<span class="headline">项目内容</span>'+
                                    '</v-card-title>'+
                                    '<v-card-text'+
                                        'style="height:75%"'+
                                        '>'+
                                        '<v-container'+
                                            'style="height:100%"'+
                                            '>'+
                                            '<v-row>'+
                                                '<v-col cols="12">'+
                                                '<v-text-field label="项目名称*" required></v-text-field>'+
                                                '</v-col>'+
                                                '<v-col cols="12" sm="6">'+
                                                '<v-text-field label="项目位置*" required></v-text-field>'+
                                                '</v-col>'+
                                                '<v-col cols="12" sm="6">'+
                                                    '<v-menu'+
                                                        'v-model="dateMenu"'+
                                                        ':close-on-content-click="false"'+
                                                        ':nudge-right="40"'+
                                                        'transition="scale-transition"'+
                                                        'offset-y'+
                                                        'min-width="290px"'+
                                                        '>'+
                                                        '<template v-slot:activator="{ on }">'+
                                                            '<v-text-field'+
                                                                'v-model="date"'+
                                                                'label="创建日期*"'+
                                                                'readonly'+
                                                                'v-on="on"'+
                                                            '></v-text-field>'+
                                                        '</template>'+
                                                        '<v-date-picker v-model="date" @input="dateMenu = false"></v-date-picker>'+
                                                    '</v-menu>'+
                                                '</v-col>'+
                                                '<v-col cols="12" sm="6">'+
                                                    '<v-select'+
                                                    ':items=' +
                                                    '["0-10,000", "10,000-100,000", "100,000+"]'+
                                                    'label="规模"'+
                                                    'required'+
                                                    '></v-select>'+
                                                    '</v-col>'+
                                                '<v-col cols="12" sm="6">'+
                                                    '<v-autocomplete'+
                                                        ':items=' +
                                                        '["住宅", "办公", "商业", "医院", "学校", "文化建筑", "工业厂房", "展览会馆", "其他"]'+
                                                        'label="类型"'+
                                                        'multiple'+
                                                    '></v-autocomplete>'+
                                                '</v-col>'+
                                            '</v-row>'+
                                        '</v-container>'+
                                        '<small>*必填项</small>'+
                                    '</v-card-text>'+
                                    '<v-card-actions>'+
                                        '<v-spacer></v-spacer>'+
                                        '<v-btn color="blue darken-1" text @click="dialog = false">Close</v-btn>'+
                                        '<v-btn color="blue darken-1" text @click="dialog = false">Save</v-btn>'+
                                    '</v-card-actions>'+
                                '</v-card>'+
                            '</v-dialog>'+
                        '</template>'+

                    '</v-card-actions>'+
                '</v-card>'+
            '</template>',

        addProjectionCard: function(){



        }

    }

};