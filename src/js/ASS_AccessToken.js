/**
 * Created by keyuhan on 2020/1/23.
 */

var ASS_AccessToken = {

    accessToken: "",// cn-e2612534-5ecd-46e3-8cbf-170a6884247d

    expireTime: "",// 2020-01-30 23:08:55

    // get access token of bimface
    getAccessToken: function () {

        var callback = function (response) {

            if(response != ""){

                response = JSON.parse(response);

                if (response['code'] == "success") {

                    ASS_AccessToken.accessToken = response['data']['token'];

                    ASS_AccessToken.expireTime = response['data']['expireTime'];

                    console.log("AccessToken: " + ASS_AccessToken.accessToken);

                    console.log("expireTime: " + ASS_AccessToken.expireTime);
                }
            }
        };

        ASS_AccessToken.getAccessTokenByCallback(callback);
    },

    // get access token of bimface with callback function
    getAccessTokenByCallback : function (callback) {

        var oMyForm = new FormData();

        var url = "php/getAccessToken.php";

        ASS_AJAX.loadObject("POST", url, oMyForm, callback);

    }

};
