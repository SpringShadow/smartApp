/**
 * Created by keyuhan on 2020/1/23.
 */
var ASS_Model = {

    // show model by view token
    show: function (viewToken) {

        // 设置BIMFACE JSSDK加载器的配置信息
        var loaderConfig = new BimfaceSDKLoaderConfig();
        loaderConfig.viewToken = viewToken;
        // 加载BIMFACE JSSDK加载器
        BimfaceSDKLoader.load(loaderConfig, successCallback, failureCallback);
        // 加载成功回调函数
        function successCallback(viewMetaData) {
            // 创建WebApplication，直接显示模型或图纸
            var dom4Show = document.getElementById('domId');
            new Glodon.Bimface.Application.WebApplicationDemo(viewMetaData, dom4Show);
        }
        // 加载失败回调函数
        function failureCallback(error) {
            console.log(error);
        }
    },


    // show model by file id
    showByFileId: function (fileId) {

        ASS_ViewToken.getViewTokenByFileId(fileId);

    }
}