
var ASS_AJAX = {

    isShowProgress: false,

    progressTopic:"",

    progressBy:'loading',

    totalPiecesSize:1,

    progressSize:0,

    progressLast:0,

    progressStartTime:0,

    index:0,

    loadObject: function (type, url, date, callback) {

        var xmlhttp = this.getXmlHttpObject();

        xmlhttp.onreadystatechange = stateChange;

        //down event
        xmlhttp.addEventListener("progress", updateProgress, false);

        xmlhttp.addEventListener("load", transferComplete, false);

        xmlhttp.addEventListener("error", transferFailed, false);

        xmlhttp.addEventListener("abort", transferCanceled, false);

        //upload event
        xmlhttp.upload.addEventListener("progress", updateProgress, false);

        xmlhttp.upload.addEventListener("load", transferComplete, false);

        xmlhttp.upload.addEventListener("error", transferFailed, false);

        xmlhttp.upload.addEventListener("abort", transferCanceled, false);

        switch (type) {

            case "GET_FALSE":

                xmlhttp.open("GET", url, false);                   //同步方式请求

                break;

            case "GET":

                xmlhttp.open("GET", url, true);                     //异步方式请求

                break;

            case "POST":

                xmlhttp.open("POST", url, true);                    //用给定的 URL 来打开打开这个 XMLHTTP 对象

                break;

            default:

                xmlhttp.open("GET", url, true);                     //用给定的 URL 来打开打开这个 XMLHTTP 对象
        }

        xmlhttp.send(date);                                         //向服务器发送 HTTP 请求

        //------------------------------------------function----------------------------------------
        function stateChange() {

            if (xmlhttp.readyState == 4 || xmlhttp.readyState == 200) {

                if (typeof(callback) == "function") {

                    callback(xmlhttp.response);

                    // Complete  
                    if (ASS_AJAX.isShowProgress) {

                        ASS_AJAX.index ++;

                        console.log('progressSize: ' + ASS_AJAX.progressSize + ', index: ' +  ASS_AJAX.index + ' ,' + ASS_AJAX.totalPiecesSize);

                        if(ASS_AJAX.index == ASS_AJAX.totalPiecesSize){

                            ASS_Cursor.showProgress(100, "topic", ASS_AJAX.progressTopic);

                            ASS_AJAX.isShowProgress = false;

                        }

                        else if (ASS_AJAX.progressBy == "loading"){

                            var percentage = Math.round(ASS_AJAX.index / ASS_AJAX.totalPiecesSize * 100);

                            ASS_Cursor.showProgress(percentage, 'topic_and_progress', ASS_AJAX.progressTopic);

                            ASS_AJAX.progressSize = percentage;

                        }

                    }

                }

            }

        }

        function updateProgress(e){

            var progressTextType = '';

            if(ASS_AJAX.isShowProgress) {

                var percentage = 0;

                switch (ASS_AJAX.progressBy){

                    case "loading":

                        progressTextType = 'topic_and_progress';

                        if (e.lengthComputable) {//sometimes loaded and total is 249

                            var piecePercentage = Math.round((e.loaded / e.total) * 100 / ASS_AJAX.totalPiecesSize);

                            percentage = ASS_AJAX.progressSize + piecePercentage;

                        }

                        break;

                    case "time":

                        progressTextType = 'topic';

                        var time = (new Date().getTime() - ASS_AJAX.progressStartTime) / 1000;

                        percentage = Math.round(time % 100);

                        break;

                    default:
                }



                if (percentage > ASS_AJAX.progressLast) {

                    ASS_Cursor.showProgress(percentage, progressTextType, ASS_AJAX.progressTopic);       //show progress on cursor canvas

                    ASS_AJAX.progressLast = percentage;

                }
            }
        }

        function transferComplete(e) {

            if (e.lengthComputable && e.total > 252) {              //sometimes loaded and total is 249


            }
        }

        function transferFailed(e) {

            console.log("An error occurred while transferring the file.");
        }

        function transferCanceled(e) {

            console.log("The transfer has been canceled by the user.");
        }
    },

    getXmlHttpObject: function () {

        var xmlHttp = null;

        try {

            // Firefox, Opera 8.0+, Safari
            xmlHttp = new XMLHttpRequest();

        }
        catch (e) {

            // Internet Explorer
            try {

                xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");

            }

            catch (e) {

                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
        }

        return xmlHttp;
    },

    // when upload begin
    setProgressValue: function (progressValues) {

        if(progressValues['restart']){

            ASS_AJAX.progressSize = 0;

            ASS_AJAX.totalPiecesSize = 1;

            ASS_AJAX.progressLast = 0;

            ASS_AJAX.index = 0;
        }

        ASS_AJAX.isShowProgress = progressValues['isShowProgress'];

        ASS_AJAX.progressTopic = progressValues['progressTopic'];

        ASS_AJAX.progressBy = progressValues['progressBy'];

        switch (ASS_AJAX.progressBy){

            case "loading":

                ASS_AJAX.progressSize = progressValues['progressSize'];

                ASS_AJAX.totalPiecesSize = progressValues['totalPieces'];

                break;

            case "time":

                if (progressValues['startTime'] ){

                    ASS_AJAX.progressStartTime = progressValues['startTime'];

                }

                break;

            default:

        }
        
    }
};