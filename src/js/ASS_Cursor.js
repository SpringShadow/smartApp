/**
 * Created by Administrator on 2015/11/9.
 */
var ASS_Cursor = {
    canvas      :   null,
    ctx         :   null,
    aliass      :   [],                         //所有绘图对象的索引名称
    objects     :   {},                         //所有绘图对象

    mousemode   :   "",
    drawtype    :   "",
    selobject   :   "",

    alias       :   "cursor",
    stroked     :   false,                      //不填充为true
    closed      :   true,                       //封闭为true
    color       :   "#3399FF",
    fillcolor   :   "#ffffff",                 //最浅的颜色
    light_color :   "#3399FF",                 //选择时的颜色
    deep_color  :   "#246666",                 //选择时的深色
    linewidth   :   2,
    alpha       :   1.0,
    radius      :   4,

    scaleSize   :   1.0,

    doprogress  :   false,//whether is doing circle progress?

    create: function () {
        var cursor = document.createElement("canvas");
        cursor.className = "ass_cursor_canvas";
        var parent = document.getElementById("ass_body");
        parent.appendChild(cursor);
        var ctx = cursor.getContext('2d');

        this.canvas = cursor;
        this.ctx = ctx;
        this.resize();
    },

    resize: function () {
        var scalesize = ASS_Cursor.scaleSize;
        var canvas = this.canvas;
        var parent = canvas.parentNode;
        canvas.width = scalesize * parent.offsetWidth;
        canvas.height = scalesize * parent.offsetHeight;
        this.rendercanvas();
    },

    //build cursor object to cursor objects
    addcursor: function (pt, type, isModelPoint) {
        //----------------------------------------build alias-------------------------------------------
        var alias = this.alias;
        switch (type) {
            case "start":                 //第一个
                alias = alias + "_0";
                break;
            case "second":                //第二个
                alias = alias + "_1";
                break;
            case "third":                 //第三个
                alias = alias + "_2";
                break;
            case "add":                   //随光标增加
                alias = this.addalias(alias);
                break;
            default :
                alias = alias + "_0";
        }

        //----------------------------------------build pts---------------------------------------------
        var mpt;
        if (isModelPoint) {
            mpt = pt;
        } else {
            mpt = Ass_Camera.get3D(pt[0], pt[1], 0, sPlane.create([0, 0, 0], [0, 0, 1]));
        }
        var pts = [[mpt, this.radius]];  //save model point of cursor
        //--------------------------------------build object--------------------------------------------
        var object = this.objects[alias];
        if (!object) {
            this.aliass.push(alias);
            this.objects[alias] = {
                alias: alias,
                pts: pts,
                stroked: this.stroked,//不填充为true
                closed: this.closed,//封闭为true
                color: this.color,
                fillcolor: this.fillcolor,
                linewidth: this.linewidth,
                alpha: this.alpha
            }
        } else {
            object.pts = pts;
        }
    },

    //build model object to cursor objects
    addCorner: function (mpt, type, color, randomcolor, fillcolor, corner_alias) {
        //----------------------------------------build alias-------------------------------------------
        var alias = "corner", radius = this.radius, linewidth = this.linewidth;
        switch (type) {
            case "add":                   //随光标增加
                alias = this.addalias(alias);                   //corner
                break;
            case "plane_origin":
                alias = "corner_planeOrigin";                   //sketch plane origin
                radius = 1.2 * this.radius;
                linewidth = 3;
                break;
            case "face_center":
                alias = this.addalias("corner_faceCenter");     //mesh face center
                radius = 1.0 * this.radius;
                break;
            case "plane_intersect_add":
                alias = this.addalias("plane_intersect");       //plane intersect with sketch objects
                radius = 0.3 * this.radius;
                break;
            case "mesh_intersect_add":
                alias = this.addalias("corner_meshIntersect");  //mesh intersect
                radius = 1.0 * this.radius;
                linewidth *= 1.5;
                break;
            case "surface_divide_add":
                alias = this.addalias("divide_intersect");        //surface divide
                radius = 0.3 * this.radius;
                break;
            default :
                alias = alias + "_0";
        }

        //----------------------------------------build pts---------------------------------------------
        var pts = [[mpt, radius]];  //save model point of cursor

        //--------------------------------------build object--------------------------------------------
        var object = this.objects[alias];
        if (!object) {
            this.aliass.push(alias);
            this.objects[alias] = {
                alias: alias,
                corneralias: corner_alias,
                pts: pts,
                stroked: this.stroked,                      //不填充为true
                closed: this.closed,                       //封闭为true
                color: color ? color : ASS_Cursor.color,      //color for default
                randomcolor: randomcolor,                       //random color for pick
                fillcolor: fillcolor ? fillcolor : ASS_Cursor.fillcolor,
                linewidth: linewidth,
                alpha: this.alpha
            }
        } else {
            object.pts = pts;
        }
    },

    buildAxis: function () {
        var alias = "axis";
        var stroked = true;
        var closed = false;
        var linewidth = 2;
        var alpha = 0.8;
        var sum = Ass_Grid.sum * Ass_Grid.unit;
        var object_x = this.objects[alias + "_x"];
        var object_y = this.objects[alias + "_y"];
        var object_z = this.objects[alias + "_z"];
        if (!object_x) {
            this.aliass.push(alias + "_x");
            this.objects[alias + "_x"] = {
                alias: alias + "_x",
                pts: [[0, 0, 0], [sum, 0, 0], "end"],
                stroked: stroked,//不填充为true
                closed: closed,//封闭为true
                color: Ass_constant.red,
                fillcolor: "",
                linewidth: linewidth,
                alpha: alpha
            }
        } else {
            object_x.pts = [[0, 0, 0], [sum, 0, 0], "end"];
        }
        if (!object_y) {
            this.aliass.push(alias + "_y");
            this.objects[alias + "_y"] = {
                alias: alias + "_y",
                pts: [[0, 0, 0], [0, sum, 0], "end"],
                stroked: stroked,//不填充为true
                closed: closed,//封闭为true
                color: Ass_constant.green,
                fillcolor: "",
                linewidth: linewidth,
                alpha: alpha
            }
        } else {
            object_y.pts = [[0, 0, 0], [0, sum, 0], "end"];
        }
        if (!object_z) {
            this.aliass.push(alias + "_z");
            this.objects[alias + "_z"] = {
                alias: alias + "_z",
                pts: [[0, 0, 0], [0, 0, sum], "end"],
                stroked: stroked,//不填充为true
                closed: closed,//封闭为true
                color: Ass_constant.blue,
                fillcolor: "",
                linewidth: linewidth,
                alpha: alpha
            }
        }
        else {
            object_z.pts = [[0, 0, 0], [0, 0, sum], "end"];
        }
    },

    buildWindowPick: function (pts) {
        var alias = "windowpick_0";
        var stroked = false;
        var closed = true;
        var linewidth = 2;
        var alpha = 0.4;
        var object = this.objects[alias];
        if (!object) {
            this.aliass.push(alias);
            this.objects[alias] = {
                alias: alias,
                pts: pts,
                stroked: stroked,//不填充为true
                closed: closed,//封闭为true
                color: Ass_constant.light_color,
                fillcolor: "",
                linewidth: linewidth,
                alpha: alpha
            }
        } else {
            object.pts = pts;
        }
    },

    buildLineDash: function (pts) {
        var alias = "linedash_0";
        var stroked = true;
        var closed = false;
        var linewidth = 2;
        var alpha = 1;
        var object = this.objects[alias];
        //pts = Ass_Camera.getScreenPts(pts);
        if (!object) {
            this.aliass.push(alias);
            this.objects[alias] = {
                alias: alias,
                pts: pts,
                stroked: stroked,//不填充为true
                closed: closed,//封闭为true
                color: Ass_constant.light_color,
                fillcolor: "",
                linewidth: linewidth,
                alpha: alpha
            }
        } else {
            object.pts = pts;
        }
    },

    //加载object
    addObject: function (alias, out_pts, edit_pts, texts, color, linewidth, alpha) {
        var object = {
            alias: alias,
            vertices: out_pts,
            edit_pts: edit_pts,
            boundary: null,//Ass_Channel_Canvas.getPtsBoundary(out_pts),
            texts: texts,
            color: color,
            linewidth: linewidth,
            alpha: alpha,
            graph: false
            //stroked         :    stroked,//不填充为true
            //closed          :    false,//封闭为true
        };
        //Ass_Channel_Pick.setObjectColorRandom(object);
        this.aliass.push(alias);
        this.objects[alias] = object;
    },

    //更新object
    changeObject: function (alias, out_pts, edit_pts, texts, color, linewidth, alpha, graph) {
        var object = this.objects[alias];
        object.vertices = out_pts ? out_pts : object.vertices;
        object.edit_pts = edit_pts ? edit_pts : object.edit_pts;
        object.boundary = null;//Ass_Channel_Canvas.getPtsBoundary(out_pts);
        object.texts = texts ? texts : object.texts;
        object.color = color ? color : object.color;
        object.linewidth = linewidth ? linewidth : object.linewidth;
        object.alpha = alpha ? alpha : object.alpha;
        object.graph = graph ? graph : object.graph;
        //object.stroked      =   stroked?stroked:object.stroked;
        //object.closed       =   false;
        //Ass_Channel_Pick.setObjectColorRandom(object);
        this.objects[alias] = object;
    },

    //绘制所有cursor object
    rendercanvas: function (dopick) {//dopick is true for pick corner
        if (this.doprogress) {
            return null;
        }
        if (this.aliass && this.aliass.length > 0) {
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

            //---------------------------------sort:mesh intersects is draw on top----------------------
            var sort_indexs = [], back_indexs = [], top_indexs = [];
            for (var n = 0; n < this.aliass.length; n++) {
                if (this.aliass[n].indexOf('meshIntersect_') >= 0) {
                    top_indexs.push(n);
                } else {
                    back_indexs.push(n);
                }
            }
            sort_indexs = sort_indexs.concat(back_indexs);
            sort_indexs = sort_indexs.concat(top_indexs);

            for (var k = 0; k < sort_indexs.length; k++) {
                var i = sort_indexs[k];
                var object = this.objects[this.aliass[i]];

                //----------------------------------------axis------------------------------------------
                if (this.aliass[i].indexOf("axis_") >= 0) {
                    if (Ass_constant.show_axis) {             //允许显示坐标轴
                        Ass_Draw.drawPts(this.ctx, "line", object.pts, object.stroked, object.closed, object.color, object.fillcolor, object.linewidth, object.alpha);
                    }
                }

                //--------------------------------------cursor------------------------------------------
                else if (this.aliass[i].indexOf("cursor_") >= 0) {
                    if (!dopick) {
                        var mpt = object.pts[0][0],
                            radius = object.pts[0][1],
                            pts = [[Ass_Camera.get2D(mpt[0], mpt[1], mpt[2]), radius]];
                        Ass_Draw.drawPts(this.ctx, "circle", pts, object.stroked, object.closed, object.color, object.fillcolor, object.linewidth, object.alpha);
                    }
                }

                //-------------------------------------linedash-----------------------------------------
                else if (this.aliass[i].indexOf("linedash_") >= 0) {
                    var pts = Ass_Camera.getScreenPts(object.pts);
                    Ass_Draw.drawPts(this.ctx, "line_dash", pts, object.stroked, object.closed, object.color, object.fillcolor, object.linewidth, object.alpha);

                    //}else if(this.aliass[i].indexOf("dim") >= 0){
                    //Ass_Channel_Dim.drawdim(object);
                }

                //-----------------------------------windowpick-----------------------------------------
                else if (this.aliass[i].indexOf("windowpick_") >= 0) {
                    Ass_Draw.drawPts(this.ctx, "line", object.pts, object.stroked, object.closed, object.color, object.fillcolor, object.linewidth, object.alpha);
                }

                //-------------------------------------corner-------------------------------------------
                else if (this.aliass[i].indexOf('corner_') >= 0) {
                    var mpt = object.pts[0][0],
                        radius = object.pts[0][1],
                        pts = [[Ass_Camera.get2D(mpt[0], mpt[1], mpt[2]), radius]],
                        color = object.color,
                        stroked = false,
                        fillcolor = object.fillcolor,
                        type = this.aliass[i].indexOf('planeOrigin') >= 0 || this.aliass[i].indexOf('faceCenter') >= 0 ?
                            "dot_cross" : this.aliass[i].indexOf('meshIntersect') >= 0 ? "intersect" : "circle",
                        alpha = object.alpha;
                    //do pick
                    if (dopick) {
                        pts[0][1] = Ass_Channel_Pick.picksize / 2;//radius
                        color = object.randomcolor;
                        stroked = false;
                        fillcolor = object.randomcolor,
                            type = "circle";
                    }
                    //picked?
                    else if (Ass_Sketch_Canvas_Pick.pickCursors.indexOf(object.corneralias) >= 0) {
                        pts[0][1] = (this.aliass[i].indexOf('meshIntersect') >= 0 ? 0.8 : 1) * Ass_Channel_Cursor.pickedradius;//radius
                        color = Ass_constant.red;
                        stroked = false;
                        fillcolor = Ass_constant.red;
                    }
                    //normal
                    else if (this.aliass[i].indexOf("faceCenter") >= 0) {
                        color = object.color;
                        alpha = Ass_Sketch.getColorAlpha(object.corneralias, "facecenters");
                    }
                    Ass_Draw.drawPts(this.ctx, type, pts, stroked, object.closed, color, fillcolor, object.linewidth, alpha);
                    //show meshIntersect's corner's text
                    if (this.aliass[i].indexOf('meshIntersect') >= 0) {
                        var text = object['corneralias'].split('/')[1],
                            text_pt = pts[0][0],
                            text_height = this.radius;//4
                        this.showCursorText(text, text_pt, text_height, null, "cyen");
                    }
                }

                //---------------------------planeintersect or sketchintersect---------------------------
                else if (this.aliass[i].indexOf('intersect_') >= 0) {
                    if (!dopick) {
                        var mpt = object.pts[0][0],
                            radius = object.pts[0][1],
                            pts = [[Ass_Camera.get2D(mpt[0], mpt[1], mpt[2]), radius]];
                        Ass_Draw.drawPts(this.ctx, "intersect", pts, object.stroked, object.closed, object.color, object.fillcolor, object.linewidth, object.alpha);
                    }
                }
            }
        } else {
            this.clearall();
        }
    },

    clearall: function () {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.aliass = [];
        this.objects = {};
    },

    getlastalias: function (type) {
        var aliass = this.aliass;
        var typenames = [];
        for (var i = 0; i < aliass.length; i++) {
            if (aliass[i].indexOf(type) >= 0) {
                typenames.push(aliass[i]);
            }
        }
        if (typenames.length == 0) {
            return "";
        } else {
            return typenames[typenames.length - 1];
        }
    },

    //set new alias by next sort
    addalias: function (typename) {
        return typename + "_" + (getLastIndex(this.getAliasArray(typename)) + 1);

        function getLastIndex(aliass) {
            var indexs = [-1];
            for (var i = 0; i < aliass.length; i++) {
                var split = aliass[i].split('_');
                if (split && split.length > 1) {
                    indexs.push(parseInt(split[split.length - 1]));//"corner_planeCenter_1"
                }
            }
            indexs.sort(Ass_constant.sortNumber);
            return indexs[indexs.length - 1];
        }
    },

    //get all the typename of aliass
    getAliasArray: function (typename) {
        var aliass = this.aliass;
        var typenames = [];
        for (var i = 0; i < aliass.length; i++) {
            if (aliass[i].indexOf(typename + "_") >= 0) {
                typenames.push(aliass[i]);
            }
        }
        return typenames;
    },

    delalias: function (type) {
        var aliass = this.aliass;
        var newaliass = [];
        var newobjects = {};
        for (var i = 0; i < aliass.length; i++) {
            if (aliass[i].indexOf(type) < 0) {
                newaliass.push(aliass[i]);
                newobjects[aliass[i]] = this.objects[aliass[i]];
            }
        }
        this.aliass = newaliass;
        this.objects = newobjects;
    },

    showCursorText: function (text, text_pt, text_height, color, font, type) {
        font = !font ? "palmsprings" : font;
        color = !color ? Ass_constant.yellow : color;
        if (text.isChinese()) {                   //包含中文
            text_height *= 2;   //corner radius
        } else {
            text_height *= 3;
        }
        text_pt = [[text_pt[0], text_pt[1] - 1.2 * text_height]];
        Ass_Channel_Draw.drawText(ASS_Cursor.ctx, text, text_height, text_pt, color, font, false, type);
    },

    // show circle progress on sketch canvas
    showProgress: function (progress, type, topic) {

        // console.log(progress);

        //--------------------------------------get center of canvas------------------------------------
        var w = this.canvas.width,
            h = this.canvas.height,
            center = [w / 2, h / 2, 0],
            r = 80;

        //-------------------------------------draw circle on progress----------------------------------
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

        //o to 360 circle
        this.ctx.beginPath();
        this.ctx.arc(center[0], center[1], r + 3, 0, 2 * Math.PI);
        //this.ctx.lineWidth = 1;
        //this.ctx.strokeStyle = Ass_constant.light_color;
        //this.ctx.stroke();
        this.ctx.fillStyle = "rgba(255 , 255 , 255 , 0.6)";
        this.ctx.fill();

        //progress circle
        var text = "";
        if (typeof progress == "number") {
            var counterClockwise = false,
                startAngle = 1.5 * Math.PI,
                endAngle = (startAngle + progress / 100 * 2 * Math.PI) % (2 * Math.PI);
            if (progress == 100) {
                startAngle = 0;
                endAngle = 2 * Math.PI;
            }
            this.ctx.beginPath();
            this.ctx.arc(center[0], center[1], r, startAngle, endAngle, counterClockwise);
            this.ctx.lineWidth = 5;
            this.ctx.strokeStyle = ASS_Cursor.light_color;
            this.ctx.stroke();

            this.ctx.font = '20px palmsprings';
            this.ctx.textAlign = 'center';
            this.ctx.textBaseline = 'middle';
            this.ctx.fillStyle = ASS_Cursor.light_color;

            var offset_y = 0;

            // show time or progress?
            switch (type){
                case "time":
                    text = (progress / 10).toFixed(2) + "m";
                    break;
                case "topic_and_progress":
                    this.ctx.fillText(topic, center[0], center[1] - 15);
                    text = progress + "%";
                    offset_y = 15;
                    break;
                case "topic":
                    text = topic;
                    break;
                default:
                    text = progress + "%";
            }
        } else {
            text = progress;
        }

        //-------------------------------------draw text of progress------------------------------------
        this.ctx.fillText(text, center[0], center[1] + offset_y);
    },

    hiddenProgress: function () {
        ASS_Cursor.rendercanvas();
    }
};