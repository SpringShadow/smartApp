<?php
/**
 * Created by PhpStorm.
 * User: keke
 * Date: 2020/2/8
 * Time: 0:11
 */

    include('commonFunction.php');

    $cf = new commonFunction();


    $modelId = $cf->test_input($_POST["modelId"]);


    $data['code'] = 'failure';

    $data['msg'] = '准备删除图纸';

    $data['data'] = '';


    $count = 0;

    // get children
    $children = $cf->getValueByKey('m_children', $con, $_MODEL_TABLE, 'm_id', $modelId);

    if($children != null && $children != ''){

        $childrenArray = json_decode(htmlspecialchars_decode($children), true);

        // delete all children
        foreach ($childrenArray as $m_id) {

            $sql = "DELETE FROM {$_MODEL_TABLE} WHERE m_id = '$m_id'";

            $results = mysqli_query($con, $sql);

            if($results){

                $count ++;

            }

        }

    }

    // delete current id in sql
    $sql = "DELETE FROM {$_MODEL_TABLE} WHERE m_id = '$modelId'";

    $results = mysqli_query($con, $sql);

    if($results){

        $count ++;

        $data['code'] = 'success';

        $data['msg'] = '删除 '. $count .' 个图纸成功';

    }

    mysqli_close($con);

    echo json_encode($data);

?>