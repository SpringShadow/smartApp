<?php
/**
 * Created by PhpStorm.
 * User: keke
 * Date: 2020/2/13
 * Time: 20:48
 */

    include('commonFunction.php');

    $cf = new commonFunction();


    $p_id = $cf->test_input($_POST["p_id"]);

    $event_id = $cf->test_input($_POST["event_id"]);

    $event_name = $cf->test_input($_POST["event_name"]);

    $event_start_date = $cf->test_input($_POST["event_start_date"]);

    $event_start_time = $cf->test_input($_POST["event_start_time"]);

    $event_end_date = $cf->test_input($_POST["event_end_date"]);

    $event_end_time = $cf->test_input($_POST["event_end_time"]);

    $event_color = $cf->test_input($_POST["event_color"]);

    $event_type = $cf->test_input($_POST["event_type"]);

    $event_location = $cf->test_input($_POST["event_location"]);

    $event_creator_id = $cf->test_input($_POST["event_creator_id"]);

    $edit_mode = $cf->test_input($_POST["edit_mode"]);// edit or delete


    // response data

    $data['code'] = 'failure';

    $data['msg'] = '准备修改事件';

    $data['data'] = array();


    if(!empty($p_id) && !empty($event_name) && $event_start_date) {

        $new_event = array(

            'name'=>$event_name,

            'start'=>$event_start_date . ' ' . $event_start_time,

            'end'=>$event_end_date . ' ' . $event_end_time,

            'color'=>$event_color,

            'creator_id'=>$event_creator_id,

            'type'=>$event_type,

            'location'=>$event_location,

            'id' => $edit_mode == 'new' && empty($event_id) ? 'tuku_e_' . $cf->random(4) : $event_id,

        );

        // get events
        $events = $cf->getValueByKey('p_events', $con, $_PROJECTION_TABLE, 'p_id', $p_id);

        if($events != null && $events != ''){

            // get events array
            $eventsArray = json_decode(htmlspecialchars_decode($events), true);

        } else {

            $eventsArray = array();

        }

        if($edit_mode == 'new'){

            $new_eventsArray = $eventsArray;

            array_push($new_eventsArray, $new_event);// push new event to array

        } else {

            $new_eventsArray = array();

            foreach ($eventsArray as $event) {

                if($event['id'] == $event_id){

                    if($edit_mode == 'edit'){

                        array_push($new_eventsArray, $new_event);

                    } else if($edit_mode == 'delete'){

                        // not do

                    }


                } else {

                    array_push($new_eventsArray, $event);

                }

            }

        }

        $eventsStr = htmlspecialchars(json_encode($new_eventsArray, JSON_UNESCAPED_UNICODE));

        // update value to sql
        $sql = "UPDATE {$_PROJECTION_TABLE} SET p_events='$eventsStr' WHERE p_id = '$p_id'";

        $results = mysqli_query($con, $sql);

        if($results){

            $data['code'] = 'success';

            $data['msg'] = '修改事件成功';

            $data['data'] = array(

                'timeline' => $cf->getProjectionTimeLine($con, $_PROJECTION_TABLE, $p_id, $new_eventsArray),

            );

        } else {

            $data['msg'] = '修改事件失败';

        }


    } else {

        $data['code'] = 'failure';

        $data['msg'] = '上传数据错误';

    }

    mysqli_close($con);

    echo json_encode($data);

?>