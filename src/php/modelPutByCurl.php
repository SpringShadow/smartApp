<?php
/**
 * Created by PhpStorm.
 * User: keke
 * Date: 2020/2/5
 * Time: 14:05
 */

    include('commonFunction.php');

    $cf = new commonFunction();


    $Authorization = $cf->get_access_token();

    $m_id = $cf->test_input($_POST["modelId"]);

    $fileData = $cf->test_input($_POST["file"]);

    $fileSize = $cf->test_input($_POST["fileSize"]);

    $fileName = $cf->test_input($_POST["fileName"]);

    $projectionId = $cf->test_input($_POST["projectionId"]);

    $fileType = substr($fileName,-4);// .dwg


    $header = array(

        'Authorization: ' . $Authorization,

        'Content-type: application/json',

    );


    $data['code'] = 'failure';
    
    $data['msg'] = '准备上传图纸';

    $data['data'] = array();


    // create m_id
    // $m_id = 'tuku_m_' . $cf->random(16);

    $path = $_FILES['file']['tmp_name'];

    // upload by put
    $url = 'https://file.bimface.com/upload?name=' . $m_id . $fileType;

    $result = upload_file_to_bimface($path, $url, $header);

    /*// upload by policy
    $fileUploadName = $m_id . $fileType;

    // $result = putModelByPolicy($cf, $path, $fileUploadName, $header);*/

    $resultArray = json_decode($result, true);


    if($resultArray['code'] == "success") {

        // get file id and status

        $fileId = $resultArray['data']['fileId'];

        $status = 'upload_ok';

        $length = $resultArray['data']['length'];

        $suffix = $resultArray['data']['suffix'];

        $now = date('Y-m-d H:i:s');

        /*// insert value to sql
        $sql = "INSERT INTO online_model (m_id,m_fileid,m_status,m_creator,m_name,m_logintime,m_lasttime,m_length,m_suffix,m_projection)

                    VALUES ('$m_id','$fileId','$status','','$fileName','$now','$now','$length','$suffix','$projectionId')";*/

        // update
        $sql = "UPDATE {$_MODEL_TABLE}
        
        SET m_fileid='$fileId', m_status='$status', m_lasttime='$now', m_length='$length', m_suffix='$suffix'
        
        WHERE m_id = '$m_id'";


        $results = mysqli_query($con, $sql);

        $data['code'] = 'success';

        $data['data'] = array(

            'fileId'=>$fileId,

            'name'=>$resultArray['data']['name'],

            'status'=>$resultArray['data']['status'],

            'length'=> $length,

            'suffix'=> $suffix,

            'time'=> $now,

        );

        // whether is insert success
        if($results) {

            $data['msg'] = '上传图纸成功';

        } else {

            die('Error: ' . mysqli_error($con));

            $data['msg'] = '写入数据库失败';

        }

        mysqli_close($con);
        
    } else {

        $data['data'] = $result;

        $data['msg'] = '上传图纸失败';

    }

    echo json_encode($data);

    /*****
     * @param $path 上传到本地的文件路径
     * @param $url 请求的 url
     * @param $uid  操作人id
     * @param $position 请求发起的位置
     * @return mixed|string
     */

    function upload_file_to_bimface($path, $url, $headers) {
        //获得文件绝对路径
        $new_path = realpath($path);
        //检测文件是否存在
        if(!file_exists($new_path)){
            return json_encode(array('code'=>'error','message'=>'upload error'));
        }
        //初始化
        $curl = curl_init();
        //检测请求版本
        if (class_exists('\CURLFile')) {
            curl_setopt($curl, CURLOPT_SAFE_UPLOAD, true);
            $data = array('file' => new \CURLFile($new_path)); //>=5.5
        } else {
            if (defined('CURLOPT_SAFE_UPLOAD')) {
                curl_setopt($curl, CURLOPT_SAFE_UPLOAD, false);
            }
            $data = array('file' => '@' . $new_path); //<=5.5
        }
        //请求地址
        curl_setopt($curl, CURLOPT_URL, $url);
        //设置hearder
        if ($headers != "") {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        } else {
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        }
        //HTTP POST，设置这个选项为一个非零值
        //curl_setopt($curl, CURLOPT_POST, 1);
        // set put mode
        curl_setopt($curl, CURLOPT_PUT, 1);
        curl_setopt($curl, CURLOPT_INFILE, fopen($new_path, 'rb')); //设置资源句柄
        curl_setopt($curl, CURLOPT_INFILESIZE, filesize($new_path));
        //在HTTP中的“POST”操作。
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        //设定是否显示头信息
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        //在HTTP请求中包含一个”user-agent”头的字符串。
        curl_setopt($curl, CURLOPT_USERAGENT, "TEST");
        //不设置超时时间
        curl_setopt($curl, CURLOPT_TIMEOUT, 0);
        //执行
        $result = curl_exec($curl);

        //删除文件
        file_exists($new_path) && unlink($new_path);

        //如果存在错误
        if (curl_errno($curl)) {
            //主动触发一个异常
            $result = curl_error($curl);
        }
        //关闭资源
        curl_close($curl);

        //返回数据
        return $result;
    }

    // upload model by policy
    function putModelByPolicy($cf, $path, $fileName, $header)
    {
        //获得文件绝对路径
        $new_path = realpath($path);

        //检测文件是否存在
        if(!file_exists($new_path)){

            return json_encode(array('code'=>'error','message'=>'upload error'));

        }

        // $fileData = array('file' => new \CURLFile($new_path)); //>=5.5

        // get policy by file name

        $policy = $cf->curl_http("https://file.bimface.com/upload/policy?name=" . $fileName, "GET", $header, null);

        $policyArray = json_decode($policy, true);

        /*echo 'host: ' . $policyArray['data']['host'] . "<br />";
        echo 'objectKey: ' . $policyArray['data']['objectKey'] . "<br />";
        echo 'policy: ' . $policyArray['data']['policy'] . "<br />";
        echo 'accessId: ' . $policyArray['data']['accessId'] . "<br />";
        echo 'callbackBody: ' . $policyArray['data']['callbackBody'] . "<br />";
        echo 'signature: ' . $policyArray['data']['signature'] . "<br />";*/

        // upload model by policy

        $url = $policyArray['data']['host'];

        $params = array(

            'name' => $fileName,

            'key' => $policyArray['data']['objectKey'],

            'policy' => $policyArray['data']['policy'],

            'OSSAccessKeyId' => $policyArray['data']['accessId'],

            'callback' => $policyArray['data']['callbackBody'],

            'signature' => $policyArray['data']['signature'],

            'success_action_status' => 200,

            'file' => $new_path, //new \CURLFile($new_path),
        );

        /*echo 'name: ' . $params['name'] . "<br />";
        echo 'key: ' . $params['key'] . "<br />";
        echo 'policy: ' . $params['policy'] . "<br />";
        echo 'OSSAccessKeyId: ' . $params['OSSAccessKeyId'] . "<br />";
        echo 'callback: ' . $params['callback'] . "<br />";
        echo 'signature: ' . $params['signature'] . "<br />";
        echo 'success_action_status: ' . $params['success_action_status'] . "<br />";
        echo 'file: ' . $params['file'] . "<br />";*/

        $putModel = $cf->curl_http($url, "POST", $header, $params);

        // $putModel = $cf->do_post($url, $header, $params);

        return $putModel;
    }

?>