<?php
/**
 * Created by PhpStorm.
 * User: keke
 * Date: 2020/2/5
 * Time: 18:11
 */

    include('commonFunction.php');

    $cf = new commonFunction();


    $Authorization = $cf->get_access_token();

    $fileName = $cf->test_input($_POST["fileName"]);

    $modelId = $cf->test_input($_POST["modelId"]);

    $fileId = $cf->test_input($_POST["fileId"]);

    $fileLength = $cf->test_input($_POST["fileLength"]);

    $fileSuffix = $cf->test_input($_POST["fileSuffix"]);

    // $fileId = $cf->getValueByKey('m_fileid',$con ,'online_model' ,'m_id' ,$modelId);


    $header = array(

        'Authorization: ' . $Authorization,

        'Content-type: application/json'
    );


    $data['code'] = 'failure';

    $data['msg'] = '准备提交转换';

    $data['data'] = '';


    $result = translateModel($cf, $fileName, $fileId, $header);

    $resultArray = json_decode($result, true);

    if($resultArray['code'] == "success") {
        
        // update status by file id

        $now = date('Y-m-d H:i:s');

        $status = 'translating';

        $sql = "UPDATE {$_MODEL_TABLE} 
                SET m_fileid='$fileId', m_status='$status', m_lasttime='$now', m_length='$fileLength', m_suffix='$fileSuffix'
                WHERE m_id = '$modelId'";
        
        $results = mysqli_query($con, $sql);


        $data['code'] = 'success';

        $data['msg'] = '提交转换成功';

        $data['data'] = array(

            'time'=>date('Y-m-d H:i:s'),

            'status'=>$resultArray['data']['status'],

        );


    } else {

        $data['msg'] = '提交转换失败';

    }

    mysqli_close($con);

    echo json_encode($data);


    // translate upload model file to bimface
    function translateModel($cf, $fileName, $fileId, $header)
    {
        $phpUrl = $_SERVER['MODEL_CALLBACK_URL'];

        $params = array(

            "callback" => $phpUrl . "modelCallback.php",

            "config" => null,

            /*"config" => array(

                "exportThumb" => true,

                "exportPdf" => false,

                "exportDrawing" => true

            ),*/

            "priority" => 2,

            "source" => array(

                "compressed" => false,

                "fileId" => $fileId,

                "rootName" => $fileName

            )
        );

        $translateModel = $cf->curl_http("https://api.bimface.com/translate", "PUT", $header, json_encode($params));

        return $translateModel;
    }

?>