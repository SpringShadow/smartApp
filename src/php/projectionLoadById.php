<?php
/**
 * Created by PhpStorm.
 * User: keke
 * Date: 2020/2/2
 * Time: 12:24
 */

    include('commonFunction.php');

    $cf = new commonFunction();


    $p_id = $cf->test_input($_POST['p_id']);

    // response data
    $data = array('code' => 'failure', 'msg' => '准备加载项目', 'data' => '');

    if (!empty($p_id)) {

        $sql = "SELECT * FROM {$_PROJECTION_TABLE} WHERE p_id = '$p_id'";

        $results = mysqli_query($con, $sql);

        if($results){

            $row = mysqli_fetch_array($results);

            $cover = $row['p_cover'] != null?

                $_SERVER['EDRAW_USERS_DIR'] . '/' . $row['p_creator_id'] . '/' . $row['p_id'] . '/' . $row['p_cover']:

                $_SERVER['EDRAW_IMAGE_DIR'] . '/icon/project_background.png';

            $values = array(

                'id'=>$row['p_id'],

                'name'=>$row['p_name'],

                'location'=>$row['p_location'],

                'date'=>$row['p_date'],

                'area'=>$row['p_area'],
    
                'type'=>$row['p_type'],

                'creator_id'=>$row['p_creator_id'],

                'cover' => $cover,//$row['p_cover'],

                'browse'=>$row['p_browsetimes'] + 1,

                'members' => $cf->getProjectionMemberNamesPics($con, $_PROJECTION_TABLE, $p_id),

            );

            if($values['id'] == $p_id){

                $data['code'] = 'success';

                $data['msg'] = '加载项目成功';

                // browse times ++
                $cf->setSelectValueAdd('p_browsetimes', $con, $_PROJECTION_TABLE, 'p_id', $p_id);

                // update lasttime by now
                $cf->updateSelectValue('p_lasttime', date('Y-m-d H:i:s'), $con, $_PROJECTION_TABLE, 'p_id', $p_id);

            } else {

                $data['code'] = 'failure';

                $data['msg'] = '项目不存在';

            }

            $data['data'] = $values;


        } else {

            die('Error: ' . mysqli_error($con));

        }

    } else {

        $data['code'] = 'failure';

        $data['msg'] = '项目不存在';

    }

    mysqli_close($con);

    echo json_encode($data);;

?>