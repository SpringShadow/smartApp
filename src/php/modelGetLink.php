<?php
/**
 * Created by PhpStorm.
 * User: keke
 * Date: 2020/2/19
 * Time: 11:39
 */

    include('commonFunction.php');

    $cf = new commonFunction();


    $modelId = $cf->test_input($_POST["modelId"]);


    $data['code'] = 'failure';

    $data['msg']  = '准备获得主图纸的ID';

    $data['data']  = array();



    $build = $cf->getValueByKey('m_build', $con, $_MODEL_TABLE, 'm_id', $modelId);

    $tech = $cf->getValueByKey('m_tech', $con, $_MODEL_TABLE, 'm_id', $modelId);

    $plan = $cf->getValueByKey('m_plan', $con, $_MODEL_TABLE, 'm_id', $modelId);

    $version = $cf->getValueByKey('m_version', $con, $_MODEL_TABLE, 'm_id', $modelId);

    $date = $cf->getValueByKey('m_createtime', $con, $_MODEL_TABLE, 'm_id', $modelId);


    // get child model name
    $childName = $cf->getValueByKey('m_name', $con, $_MODEL_TABLE, 'm_id', $modelId);

    // get link model id
    $linkId = $cf->getValueByKey('m_link', $con, $_MODEL_TABLE, 'm_id', $modelId);

    // whether is child or link?

    if($linkId != null && $linkId != ''){

        // is child

        $build = $cf->getValueByKey('m_build', $con, $_MODEL_TABLE, 'm_id', $linkId);// children build same to link!!!

        // get link model name

        $linkName = $cf->getValueByKey('m_name', $con, $_MODEL_TABLE, 'm_id', $linkId);

        // get children index

        // get children
        $children = $cf->getValueByKey('m_children', $con, $_MODEL_TABLE, 'm_id', $linkId);

        $childrenArray = json_decode(htmlspecialchars_decode($children), true);

        // push open times to time line array

        $index = 1;

        $i = 1;

        $count = count($childrenArray);

        foreach ($childrenArray as $m_id) {

            if($m_id == $modelId){

                $index = $i;

            }

            $i ++;

        }


        $data['code'] = 'success';

        $data['msg']  = '当前是一个子图纸';

        $data['data']  = array(

            'isChild'=>true,

            'linkId'=>$linkId,

            'linkName'=>$linkName,

            'childId'=>$modelId,

            'childName'=>$childName,

            'badgeTxt'=>$index,// 2


            'badge_value' => array(

                'build' => $build,

                'tech' => $tech,

                'plan' => $plan,

                'version' => $version == null ? '' : $version,

                'date' => $date,

                'countTxt'=>$count,// 5

                'topTxt'=> $tech . '-' . $plan . '-' . $version,

            )



        );

    } else {

        // get children
        $children = $cf->getValueByKey('m_children', $con, $_MODEL_TABLE, 'm_id', $modelId);

        if(empty($children)){

            $count = 0;

        } else {

            $childrenArray = json_decode(htmlspecialchars_decode($children), true);

            $count = count($childrenArray);

        }


        $data['code'] = 'success';

        $data['msg']  = '当前是一个主图纸';

        $data['data']  = array(

            'isChild'=>false,

            'linkId'=>$modelId,

            'linkName'=>$childName,

            'childId'=>null,

            'childName'=>null,

            'badgeTxt'=>'',

            // 'countTxt'=>$count,

            'badge_value' => array(

                'build' => $build,

                'tech' => $tech,

                'plan' => $plan,

                'version' => $version == null ? '' : $version,

                'date' => $date,

                'countTxt'=>$count,// 5

                'topTxt'=> '',

            )

        );


    }

    mysqli_close($con);

    echo json_encode($data);

?>