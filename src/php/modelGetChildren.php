<?php
/**
 * Created by PhpStorm.
 * User: keke
 * Date: 2020/2/18
 * Time: 16:21
 */

    include('commonFunction.php');

    $cf = new commonFunction();


    $modelId = $cf->test_input($_POST["modelId"]);


    $data['code'] = 'failure';

    $data['msg']  = '准备获得不同版本图纸集';

    $data['data']  = array();


    // get children
    $children = $cf->getValueByKey('m_children', $con, $_MODEL_TABLE, 'm_id', $modelId);

    if($children != null && $children != ''){

        $data['msg'] = '获得不同版本图纸集成功';

    } else {

        // get link(parent) model id
        $linkId = $cf->getValueByKey('m_link', $con, $_MODEL_TABLE, 'm_id', $modelId);

        if($linkId != null && $linkId != ''){

            // get children
            $children = $cf->getValueByKey('m_children', $con, $_MODEL_TABLE, 'm_id', $linkId);

            $data['msg'] = '这是一个子集';

        } else {

            $data['msg'] = '没有不同版本图纸集';

        }

    }

    // get children array
    if($children != null && $children != ''){

        $childrenItems = array();

        $childrenArray = json_decode(htmlspecialchars_decode($children), true);

        $build = $cf->getValueByKey('m_build', $con, $_MODEL_TABLE, 'm_id', $modelId);// link build to children !!!

        // push open times to time line array
        foreach ($childrenArray as $m_id) {

            $name = $cf->getValueByKey('m_version', $con, $_MODEL_TABLE, 'm_id', $m_id);

            $time = $cf->getValueByKey('m_createtime', $con, $_MODEL_TABLE, 'm_id', $m_id);

            $suffix = $cf->getValueByKey('m_suffix', $con, $_MODEL_TABLE, 'm_id', $m_id);

            // $build = $cf->getValueByKey('m_build', $con, $_MODEL_TABLE, 'm_id', $m_id);

            $tech = $cf->getValueByKey('m_tech', $con, $_MODEL_TABLE, 'm_id', $m_id);

            $plan = $cf->getValueByKey('m_plan', $con, $_MODEL_TABLE, 'm_id', $m_id);

            $version = $cf->getValueByKey('m_version', $con, $_MODEL_TABLE, 'm_id', $m_id);

            array_push($childrenItems, array(

                'id'=> $m_id,// children model id

                // 'name'=> $name,
                'name'=> $tech . '-' . $plan . '-' . $version,

                'text'=> date("Y-m-d",strtotime($time)),

                'color' => 'rgba(255,255,255,0.78)',

                'show'=> true,

                'suffix'=> $suffix,

                'badge_value'=>array(

                    'build'=>$build,

                    'tech'=>$tech,

                    'plan'=>$plan,

                    'version'=>$version,

                    'date'=>$time,

                ),

            ));

        }

        $data['code'] = 'success';

        $data['data'] = array(

            'children'=> $childrenItems,

        );

    }

    mysqli_close($con);

    echo json_encode($data);

?>