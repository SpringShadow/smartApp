<?php
/**
 * Created by PhpStorm.
 * User: keke
 * Date: 2020/2/21
 * Time: 20:14
 */

    include('commonFunction.php');

    $cf = new commonFunction();


    $modelIds = $cf->test_input($_POST["modelIds"]);

    $data['code'] = 'failure';

    $data['msg'] = '准备删除目录图纸';

    $data['data'] = '';



    $modelIdsArray = explode(',',$modelIds);

    $count = 0;

    // delete all children
    foreach ($modelIdsArray as $modelId) {

        // get children
        $children = $cf->getValueByKey('m_children', $con, $_MODEL_TABLE, 'm_id', $modelId);

        if($children != null && $children != ''){

            $childrenArray = json_decode(htmlspecialchars_decode($children), true);

            // delete all children
            foreach ($childrenArray as $m_id) {

                $sql = "DELETE FROM {$_MODEL_TABLE} WHERE m_id = '$m_id'";

                $results = mysqli_query($con, $sql);

                if($results){

                    $count ++;

                }

            }

        }

        // delete current id in sql
        $sql = "DELETE FROM {$_MODEL_TABLE} WHERE m_id = '$modelId'";

        $results = mysqli_query($con, $sql);

        if($results){

            $count ++;

        }

    }

    $data['code'] = 'success';

    $data['msg'] = '删除目录下 '. $count .' 个图纸成功';

    $data['data'] = array(

        'ids' => $modelIdsArray,

    );



    mysqli_close($con);

    echo json_encode($data);

?>