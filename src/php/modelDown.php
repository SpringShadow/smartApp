<?php
/**
 * Created by PhpStorm.
 * User: keke
 * Date: 2020/2/28
 * Time: 10:38
 */

    include('commonFunction.php');

    $cf = new commonFunction();

    $Authorization = $cf->get_access_token();

    $modelId = $cf->test_input($_POST["modelId"]);

    $userId = $cf->test_input($_POST["userId"]);

    $baseModelName = $cf->test_input($_POST["baseModelName"]);


    $fileSuffix = $cf->getValueByKey('m_suffix', $con, $_MODEL_TABLE, 'm_id', $modelId);

    $downName = $cf->getFileDownName($con, $_MODEL_TABLE , $modelId, $baseModelName);


    $isModelFile = $cf->isModelFile($fileSuffix);


    $url = '';

    // is model file to get url from bimface

    if ($isModelFile){

        $fileId = $cf->getValueByKey('m_fileid', $con, $_MODEL_TABLE, 'm_id', $modelId);

        $header = array(

            'Authorization: '.$Authorization,

            'Content-type: application/json'

        );

        $data['code'] = 'failure';

        $data['msg'] = '准备下载图纸';

        $data['data'] = array();


        $fileUrl = $cf->curl_http("https://file.bimface.com/download/url?fileId=" . $fileId . "&name=" . $downName, "GET", $header, null);

        $fileUrlArray = json_decode($fileUrl, true);

        $url = $fileUrlArray['data'];

        $is_local_web = false;

    }

    // is image(pdf) file to get rul from edraw

    else {

        // get file_id , projection_id, and

        $sql = "SELECT * FROM  {$_MODEL_TABLE} WHERE m_id='$modelId'";

        $results = mysqli_query($con, $sql);

        if($results) {

            $row = mysqli_fetch_array($results);

            // $projectionPath = $_SERVER['EDRAW_USERS_DIR'] . '/' . $row['m_creator_id'] . '/' . $row['m_projection'];

            $projectionPath = '../users/' . $row['m_creator_id'] . '/' . $row['m_projection'];

            $filePath = $projectionPath . '/' . $row['m_fileid'] . '.' . $row['m_suffix'];

            $url = $filePath;

            $is_local_web = true;

        }

    }

    if($url != ''){

        // add down line
        add_down_line($cf, $con, $_MODEL_TABLE, $modelId, $userId);

        $data['code'] = 'success';

        $data['msg'] = '获取下载图纸地址成功';

        $data['data'] = array(

            'downUrl'=>$url,
            
            'downName'=>urldecode($downName),

            'isLocalWeb'=>$is_local_web,
            
        );

    } else {

        $data['msg'] = '获取下载图纸地址失败';

    }

    mysqli_close($con);

    echo json_encode($data);


    function add_down_line($cf, $con, $_MODEL_TABLE, $modelId, $userId){

        $downTime = date('Y-m-d H:i:s');

        $downLine = $cf->getValueByKey('m_downline', $con, $_MODEL_TABLE, 'm_id', $modelId);

        if($downLine != null && $downLine != ''){

            // get events array
            $downLineArray = json_decode(htmlspecialchars_decode($downLine), true);

        } else {

            $downLineArray = array();

        }

        array_push($downLineArray, array('user' => $userId, 'time' => $downTime));

        $downLineStr = htmlspecialchars(json_encode($downLineArray, JSON_UNESCAPED_UNICODE));


        // update values to sql
        $sql = "UPDATE {$_MODEL_TABLE} SET m_downline = '$downLineStr' WHERE m_id = '$modelId'";

        $results = mysqli_query($con, $sql);

        return $results;

    }

?>