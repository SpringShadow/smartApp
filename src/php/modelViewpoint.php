<?php
/**
 * Created by PhpStorm.
 * User: keyuhan
 * Date: 2020/3/19
 * Time: 1:43
 */

    include('commonFunction.php');

    $cf = new commonFunction();

    $modelId = $cf->test_input($_POST["modelId"]);

    $modelViewpoint = $cf->test_input($_POST["viewpoints"]);


    // response data

    $data['code'] = 'failure';

    $data['msg'] = '准备获取图纸视角';

    $data['data'] = array();


    // whether is update or get

    if(empty($modelViewpoint)){

        // get

        // get events
        $modelViewpoint = $cf->getValueByKey('m_viewpoint', $con, $_MODEL_TABLE, 'm_id', $modelId);


        $data['code'] = 'success';

        $data['msg'] = '获取图纸视角成功';

        $data['data'] = array(

            'viewpoints' => htmlspecialchars_decode($modelViewpoint),

        );

    } else {

        // update values

        $sql = "UPDATE {$_MODEL_TABLE} SET m_viewpoint='$modelViewpoint' WHERE m_id = '$modelId'";

        $results = mysqli_query($con, $sql);

        if($results) {

            $data['code'] = 'success';

            $data['msg'] = '更新图纸视角成功';

            $data['data'] = array(

                'viewpoints' =>$modelViewpoint

            );

        }

    }

    mysqli_close($con);

    echo json_encode($data);

?>