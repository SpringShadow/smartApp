<?php
/**
 * Created by PhpStorm.
 * User: keke
 * Date: 2020/2/29
 * Time: 13:49
 */

    include('commonFunction.php');

    $cf = new commonFunction();

    $res_filePath = '';

    $res_fileName = '';

    if(isset($_GET["modelDownImage"])) {

        $modelDownImage = $_GET["modelDownImage"];

    }

    if(isset($_GET["modelId"])) {

        $modelId = $_GET["modelId"];

    }

    // get file_id , projection_id, and

    $sql = "SELECT * FROM  {$_MODEL_TABLE} WHERE m_id='$modelId'";

    $results = mysqli_query($con, $sql);

    if($results) {

        $row = mysqli_fetch_array($results);

        $projectionPath = '../users/' . $row['m_creator_id'] . '/' . $row['m_projection'];

        $res_filePath = $projectionPath . '/' . $row['m_fileid'] . '.' . $row['m_suffix'];

    }

    // get down name

    $res_fileName = $cf->getFileDownName($con, $_MODEL_TABLE , $modelId, $baseModelName);



    downdetails($res_filePath, $res_fileName);

    //下载指定文件的方法
    function downdetails($file_path, $file_name){
        header("Content-type:text/html;charset=utf-8");
        //$file_path="testMe.txt";
        //用以解决中文不能显示出来的问题
        //$file_name=iconv("utf-8","gb2312",$file_name);
        //$file_sub_path=$_SERVER['DOCUMENT_ROOT']."marcofly/phpstudy/down/down/";
        //$file_path=$file_sub_path.$file_name;
        //首先要判断给定的文件存在与否
        if(!file_exists($file_path)){
            echo "没有该文件文件";
            return ;
        }
        $fp=fopen($file_path,"r");
        $file_size=filesize($file_path);
        //下载文件需要用到的头
        Header("Content-type: application/octet-stream");
        Header("Accept-Ranges: bytes");
        Header("Accept-Length:".$file_size);
        Header("Content-Disposition: attachment; filename=".$file_name);
        $buffer=1024;
        $file_count=0;
        //向浏览器返回数据
        while(!feof($fp) && $file_count<$file_size){
            $file_con=fread($fp,$buffer);
            $file_count+=$buffer;
            echo $file_con;
        }
        fclose($fp);
    }

?>