<?php
/**
 * Created by PhpStorm.
 * User: keke
 * Date: 2020/2/2
 * Time: 17:29
 */

    include('commonFunction.php');

    $cf = new commonFunction();


    $p_creator_id = $cf->test_input($_POST['p_creator_id']);


    // response data
    $data = array('code' => 'failure', 'msg' => '准备加载项目', 'data' => '');

    if (!empty($p_creator_id)) {

        // get projection' ids by create

        $sql = "SELECT p_id FROM {$_PROJECTION_TABLE} WHERE p_creator_id = '$p_creator_id' ORDER BY p_date DESC";// [ASC [DESC][默认 ASC]] 按升序或降序排列

        $results = mysqli_query($con, $sql);

        if($results) {

            $createProjectionArray = array();

            while ($row = mysqli_fetch_array($results)) {

                array_push($createProjectionArray, $row['p_id']);

            }
        }

        // get projection' ids by invite

        $inviteProjection = $cf->getValueByKey('projection', $con, 'online_user', 'id', $p_creator_id);

        if($inviteProjection != null && $inviteProjection != ''){

            $inviteProjectionArray = json_decode(htmlspecialchars_decode($inviteProjection), true);

        } else {

            $inviteProjectionArray = array();

        }

        // merge array of create and invite projection

        $results_array = array_merge($createProjectionArray, $inviteProjectionArray);

        $results_array = array_values(array_unique($results_array));


        $data['code'] = 'success';

        $data['msg'] = count($results_array) == 0 ? '欢迎来到EDRAW.XYZ，赶快去新建项目吧' : '加载 ' . count($results_array) . ' 个项目';

        $data['data'] = $results_array;

    } else {

        $data['code'] = 'failure';

        $data['msg'] = '用户不存在';

    }

    mysqli_close($con);

    echo json_encode($data);

?>