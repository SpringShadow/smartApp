<?php
/**
 * Created by PhpStorm.
 * User: keyuhan
 * Date: 2020/3/13
 * Time: 23:28
 */

    include('commonFunction.php');

    $cf = new commonFunction();


    $p_authority = $cf->test_input($_POST['p_authority']);


    // response data
    $data = array('code' => 'failure', 'msg' => '准备加载项目', 'data' => '');

    if (!empty($p_authority)) {

        // get projection' ids by $p_authority

        $sql = "SELECT * FROM {$_PROJECTION_TABLE} WHERE (p_authority = '$p_authority' or p_authority = 'public_team')";//

        $results = mysqli_query($con, $sql);

        if($results) {

            $results_array = array();

            while ($row = mysqli_fetch_array($results)) {

                array_push($results_array, $row['p_id']);

            }
        }

        /*// get projection' ids by invite

        $inviteProjection = $cf->getValueByKey('projection', $con, 'online_user', 'id', $p_creator_id);

        if($inviteProjection != null && $inviteProjection != ''){

            $inviteProjectionArray = json_decode(htmlspecialchars_decode($inviteProjection), true);

        } else {

            $inviteProjectionArray = array();

        }

        // merge array of create and invite projection

        $results_array = array_merge($createProjectionArray, $inviteProjectionArray);

        $results_array = array_values(array_unique($results_array));*/

        // $results_array = $cf->getValuesByKey('p_id', $con, $_PROJECTION_TABLE, 'p_authority', $p_authority);


        $data['code'] = 'success';

        $data['msg'] = count($results_array) == 0 ? '欢迎来到 EDRAW.XYZ ，赶快去新建项目吧' : '加载 ' . count($results_array) . ' 个项目';

        $data['data'] = $results_array;

    } else {

        $data['code'] = 'failure';

        $data['msg'] = '用户不存在';

    }

    mysqli_close($con);

    echo json_encode($data);

?>