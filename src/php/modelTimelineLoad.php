<?php
/**
 * Created by PhpStorm.
 * User: keke
 * Date: 2020/2/11
 * Time: 23:51
 */


    include('commonFunction.php');

    $cf = new commonFunction();


    $modelId = $cf->test_input($_POST["modelId"]);


    // get create line
    $createLine = $cf->getValueByKey('m_createline', $con, $_MODEL_TABLE, 'm_id', $modelId);

    // get time line array
    $timeLineArray = json_decode(htmlspecialchars_decode($createLine), true);


    // get open line
    $openLine = $cf->getValueByKey('m_openline', $con, $_MODEL_TABLE, 'm_id', $modelId);

    if($openLine != null && $openLine != ''){

        $openLineArray = explode(',',$openLine);

        // push open times to time line array
        foreach ($openLineArray as $time) {

            array_push($timeLineArray, array(

                'title'=> '打开',

                'content'=> $time

            ));

        }

    }

    // get down line
    $downLine = $cf->getValueByKey('m_downline', $con, $_MODEL_TABLE, 'm_id', $modelId);

    if($downLine != null && $downLine != ''){

        // $downLineArray = explode(',',$downLine);

        $downLineArray = json_decode(htmlspecialchars_decode($downLine), true);

        // push open times to time line array
        foreach ($downLineArray as $_down) {

            array_push($timeLineArray, array(

                'title'=> '下载',

                'content'=> $_down['time'],

                'creator_imgUrl' => URLdecode($cf->getValueByKey('picture', $con, 'online_user', 'id', $_down['user'])),

                'creator' => URLdecode($cf->getValueByKey('username', $con, 'online_user', 'id', $_down['user'])),

            ));

        }

    }

    // sort array by time on ASC

    $timeLineArray = $cf->sortArrayByTime($timeLineArray, 'content', true);


    $data['code'] = 'success';

    $data['msg'] = '获得图纸时间线成功';

    $data['data'] = array(

        'timeline'=> $timeLineArray,

    );

    mysqli_close($con);

    echo json_encode($data);

?>