<?php
/**
 * Created by PhpStorm.
 * User: keke
 * Date: 2020/2/2
 * Time: 11:48
 */

    include('commonFunction.php');

    $cf = new commonFunction();


    $p_creator = $cf->test_input($_POST["p_creator"]);

    $p_creatorId = $cf->test_input($_POST["p_creator_id"]);

    $p_id = $cf->test_input($_POST["p_id"]);

    $p_name = $cf->test_input($_POST["p_name"]);

    $p_location = $cf->test_input($_POST["p_location"]);

    $p_area = $cf->test_input($_POST["p_area"]);

    $p_type = $cf->test_input($_POST["p_type"]);

    $p_date = $cf->test_input($_POST["p_date"]);

    // $p_description = $cf->test_input($_POST["p_description"]);

    // $p_image = $cf->test_input($_POST["p_image"]);

    // response data

    $data['code'] = 'failure';

    $data['msg'] = '准备项目';

    $data['data'] = array();


    if(!empty($p_creator) && !empty($p_name)) {

        // whether same projection name is exist?

        $sql = "SELECT * FROM {$_PROJECTION_TABLE} WHERE (p_creator_id = '$p_creatorId'AND p_name = '$p_name' AND p_id <> '$p_id')";

        $results = mysqli_query($con, $sql);

        $row = mysqli_fetch_array($results);

        $is_same_name = count($row) > 0 ? true : false;

        if (!$is_same_name) {

            // whether is new or update?

            $is_update = $p_id != '' ? true : false;

            $now = date('Y-m-d H:i:s');

            // update projection

            if($is_update){

                // update time
                $updatetime = $now;

                // update values to sql
                $sql = "UPDATE {$_PROJECTION_TABLE}
                    SET p_name='$p_name', p_location='$p_location', p_area='$p_area', p_type='$p_type', p_date='$p_date',p_lasttime='$updatetime'
                    WHERE p_id='$p_id'";

                $results = mysqli_query($con, $sql);

                $data['data'] = $results;

                $data = do_results($con, $results, $p_id, $is_update);

            }

            // new projection

            else {

                // create p_id
                $p_id = 'tuku_p_' . $cf->random(16);

                // login time
                $p_logintime = $now;

                // browse times
                $p_browsetimes = 0;

                $p_members = htmlspecialchars(json_encode(array($p_creatorId), JSON_UNESCAPED_UNICODE));

                // insert value to sql
                $sql = "INSERT INTO {$_PROJECTION_TABLE} (p_creator,p_creator_id,p_id,p_name,p_location,p_area,p_type,p_date,p_logintime,p_lasttime,p_browsetimes,p_members,p_authority) 
                    
                    VALUES ('$p_creator','$p_creatorId','$p_id','$p_name','$p_location','$p_area','$p_type','$p_date','$p_logintime','$p_logintime','$p_browsetimes','$p_members','team')";

                $results = mysqli_query($con, $sql);

                /*// save projection id to user's

                $cf->saveProjectionIdToUser($con, $_PROJECTION_TABLE, $p_id, $p_creatorId);*/

                // do results

                $data = do_results($con, $results, $p_id, $is_update);

                // create file on web by user id

                $cf->createDirByUserIdAndProjectionId($p_creatorId, $p_id);

            }

        } else {

            $data['code'] = 'failure';

            $data['msg'] = '项目名称已经存在';

        }



    } else {

        $data['code'] = 'failure';

        $data['msg'] = '上传数据错误';

    }

    echo json_encode($data);


    function do_results($con, $results, $p_id, $is_update){

        $msg = $is_update?'修改':'创建';

        if ($results) {

            $data['code'] = 'success';

            $data['msg'] = $msg.'项目成功';

        } else {

            die('Error: ' . mysqli_error($con));

            $data['code'] = 'failure';

            $data['msg'] = $msg.'项目失败: ' . mysqli_error($con);

        }

        $data['data'] = array('id' => $p_id,'update'=>$is_update);

        mysqli_close($con);

        return $data;

    }

?>






