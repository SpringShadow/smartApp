<?php
/**
 * Created by PhpStorm.
 * User: keke
 * Date: 2020/3/1
 * Time: 19:44
 */

    include('commonFunction.php');

    $cf = new commonFunction();

    $projectionId = $cf->test_input($_POST["projectionId"]);

    $userId = $cf->test_input($_POST["userId"]);


    $data['code'] = 'failure';

    $data['msg'] = '查找成员';

    $data['data'] = '';


    $members = $cf->getValueByKey('p_members', $con, $_PROJECTION_TABLE, 'p_id', $projectionId);

    $membersArray = json_decode(htmlspecialchars_decode($members), true);


    // whether to add member?

    if(!in_array($userId, $membersArray)){

        // add a new member to projection's members!

        array_push($membersArray, $userId);

        $membersStr = htmlspecialchars(json_encode($membersArray, JSON_UNESCAPED_UNICODE));

        // update value to sql
        $sql = "UPDATE {$_PROJECTION_TABLE} SET p_members='$membersStr' WHERE p_id = '$projectionId'";

        $results = mysqli_query($con, $sql);

        // save the projection id  to user's

        $cf->saveProjectionIdToUser($con, $_PROJECTION_TABLE , $projectionId, $userId);

        // do results

        if($results){

            $data['code'] = 'success';

            $data['msg'] = '添加新成员成功';

            $data['data'] = array(

                'members' => $cf->getProjectionMemberNamesPics($con, $_PROJECTION_TABLE, $projectionId),

            );

        } else {

            $data['code'] = 'failure';

            $data['msg'] = '添加新成员失败';

        }

    } else {

        $data['code'] = 'success';

        $data['msg'] = '查找成员成功';

        $data['data'] = array(

            'members' => $cf->getProjectionMemberNamesPics($con, $_PROJECTION_TABLE, $projectionId),

        );

    }

    mysqli_close($con);

    echo json_encode($data);

?>