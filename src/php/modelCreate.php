<?php
/**
 * Created by PhpStorm.
 * User: keke
 * Date: 2020/2/11
 * Time: 16:11
 */

    include('commonFunction.php');

    $cf = new commonFunction();


    $projectionId = $cf->test_input($_POST["projectionId"]);

    $fileName = $cf->test_input($_POST["fileName"]);

    $creatorId = $cf->test_input($_POST["creatorId"]);

    $createTime = $cf->test_input($_POST["createTime"]);

    $linkModelId = $cf->test_input($_POST["linkModelId"]);

    $modelVersion = $cf->test_input($_POST["modelVersion"]);

    $modelId = $cf->test_input($_POST["modelId"]);

    $modelBuild = $cf->test_input($_POST["modelBuild"]);

    $modelTech = $cf->test_input($_POST["modelTech"]);

    $modelPlan = $cf->test_input($_POST["modelPlan"]);


    $data['code'] = 'failure';
    
    $data['msg'] = '准备创建新图纸';
    
    $data['data'] = array();


    // whether is new or update?

    if($modelId != null && $modelId != ''){

        // is update mode

        $statusStr = 'update';

        // whether is change file?

        if($fileName != null && $fileName != ''){

            // update name version create time to sql
            $sql = "UPDATE {$_MODEL_TABLE} 
                SET m_name='$fileName', m_build='$modelBuild', m_tech='$modelTech', m_plan='$modelPlan', m_version='$modelVersion', m_createtime='$createTime',m_status='$statusStr'
                WHERE m_id='$modelId'";

        } else {

            // update no file to sql
            $sql = "UPDATE {$_MODEL_TABLE} 
                SET m_build='$modelBuild', m_tech='$modelTech', m_plan='$modelPlan', m_version='$modelVersion', m_createtime='$createTime',m_status='$statusStr'
                WHERE m_id='$modelId'";

        }

        $results = mysqli_query($con, $sql);

        if($results){

            $data['code'] = 'success';

            $data['msg'] = '更新新图纸';

            $data['data'] = array(

                'modelId' => $modelId,

                'childrenCount' => -1,// no change

                'status' => $statusStr,

                'time' => date('Y-m-d H:i:s'),

            );

        }

    } else {

        // is new mode

        // create m_id
        $m_id = 'tuku_m_' . $cf->random(16);

        $now = date('Y-m-d H:i:s');

        $statusStr = 'begin';


        // insert value to sql
        $sql = "INSERT INTO {$_MODEL_TABLE} (m_id,m_status,m_creator_id,m_name,m_logintime,m_projection,m_browsetimes,m_link,m_version,m_createtime,m_build,m_tech,m_plan) 
                        
                        VALUES ('$m_id','$statusStr','$creatorId','$fileName','$now','$projectionId',0,'$linkModelId','$modelVersion','$createTime','$modelBuild','$modelTech','$modelPlan')";

        $results = mysqli_query($con, $sql);


        // insert current(children) model id to link(parent) model id?

        if ($linkModelId != null && $linkModelId != '') {

            $modelChildren = $cf->getValueByKey('m_children', $con, $_MODEL_TABLE, 'm_id', $linkModelId);

            if ($modelChildren == null || $modelChildren == '') {

                $modelChildrenArray = array();

            } else {

                $modelChildrenArray = json_decode(htmlspecialchars_decode($modelChildren), true);

            }

            array_push($modelChildrenArray, $m_id);// push current id to link(parent) model id

            $childrenString = htmlspecialchars(json_encode($modelChildrenArray, JSON_UNESCAPED_UNICODE));

            /*// update value to sql
            $sql = "UPDATE {$_MODEL_TABLE} SET m_children ='$childrenString' WHERE m_id = '$linkModelId'";

            $linkResults = mysqli_query($con, $sql);*/


            // update link(parent) model timeline

            // get create line
            $createLine = $cf->getValueByKey('m_createline', $con, $_MODEL_TABLE, 'm_id', $linkModelId);

            // get time line array
            $timeLineArray = json_decode(htmlspecialchars_decode($createLine), true);


            // push new item
            array_push($timeLineArray, array(

                'title' => '创建' . $modelVersion,

                'content' => $now,

            ));

            $timeLineString = htmlspecialchars(json_encode($timeLineArray, JSON_UNESCAPED_UNICODE));

            // update timeline and children values to sql
            $sql = "UPDATE {$_MODEL_TABLE} 
                SET m_createline='$timeLineString' , m_children ='$childrenString'
                WHERE m_id='$linkModelId'";

            $linkResults = mysqli_query($con, $sql);

            if ($linkResults) {

                $data['code'] = 'success';

                $data['msg'] = '创建或修改不同版本图纸';

                $data['data'] = array(

                    'modelId' => $m_id,

                    'childrenCount' => count($modelChildrenArray),

                    'status' => $statusStr,

                    'time' => date('Y-m-d H:i:s'),

                );

            }

        } else if ($results) {

            $data['code'] = 'success';

            $data['msg'] = '创建或修改图纸';

            $data['data'] = array(

                'modelId' => $m_id,

                'childrenCount' => 0,// no children

                'status' => $statusStr,

                'time' => date('Y-m-d H:i:s'),

            );

        }
    }

    mysqli_close($con);

    echo json_encode($data);

?>
