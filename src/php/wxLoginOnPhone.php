<?php
/**
 * Created by PhpStorm.
 * User: keyuhan
 * Date: 2018/8/10
 * Time: 15:55
 */

    /**
     * 微信公众号开发获取客户资料，手机端识别二维码登录 到 iframe
     */
    header("Content-type: text/html; charset=utf-8");

    include("commonFunction.php");

    $cf = new commonFunction();

    $_ID = "wxa83f724a3b8d25a5";

    $_SECRET = "2a85458f0e06901a3187f2a9f3a5df2a";// secret key

    $_STATE = "kCC0gQaJUPn1I4WwGa1iQupNNVylxIF2";

    $code = $cf->test_input($_GET["code"]);

    $state = $cf->test_input($_GET["state"]);

    $p_id = $cf->test_input($_GET["pid"]);

    //    echo $code.'<br>';
    //    echo $state.'<br>';

    if ($code && $state == $_STATE) {

        $url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=$_ID&secret=$_SECRET&code=$code&grant_type=authorization_code";

        $access_token_str = file_get_contents($url);

        // get user info
        if ($access_token_str) {

            $access_array = json_decode($access_token_str);

            $access_token = $access_array->access_token;;

            $openid = $access_array->openid;

            $access_url = "https://api.weixin.qq.com/sns/userinfo?access_token=$access_token&openid=$openid&lang=zh_CN";

            $wx_user_info = file_get_contents($access_url);

            $wx_user_info = str_replace("\\/", "/", $wx_user_info);

            // echo $wx_user_info;

            $wx_user_array = json_decode($wx_user_info);

            $nickName = $wx_user_array->nickname;

            $headImgUrl = URLEncode($wx_user_array->headimgurl);// change url

            $unionId = $wx_user_array->unionid;

            $sex = $wx_user_array->sex;

            $country = $wx_user_array->country;

            $province = $wx_user_array->province;

            $city = $wx_user_array->city;

            $from = "EDRAW_ONLINE_PHONE";

            if (!empty($nickName) && !empty($unionId)) {

                // insert user info to sql
                include("wxWelcomeBehind.php");

                if($p_id != ''){

                    echo '<script type=text/javascript>top.window.location.href="' . $_LOGIN_CALLBACK_URL . '/projection?id=' . $p_id . '&user=' . $userId . '"</script>';

                } else {

                    echo '<script type=text/javascript>top.window.location.href="' . $_LOGIN_CALLBACK_URL . '/user?id=' . $userId . '"</script>';

                }

                // echo '<script type=text/javascript>top.location.href="https://www.edraw.xyz/test/tuku/index.html#/user?id='.$userId.'"</script>';

                // echo '<script type=text/javascript>top.location.href="http://localhost:1111/#/user?id='.$userId.'"</script>';
            }

        }

    } else {

        die('Error: ' . mysqli_error());

    }

?>