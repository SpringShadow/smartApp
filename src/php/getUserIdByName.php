<?php
/**
 * Created by PhpStorm.
 * User: keke
 * Date: 2020/2/9
 * Time: 20:50
 */

    include('commonFunction.php');

    $cf = new commonFunction();

    $userName = $cf->test_input($_POST['userName']);

    // response data
    $data = array('code' => 'failure', 'msg' => '查询用户ID和头像', 'data' => '');

    if (!empty($userName)) {

        $sql = "SELECT * FROM online_user WHERE username = '$userName' ";// [ASC [DESC][默认 ASC]] 按升序或降序排列

        $results = mysqli_query($con, $sql);

        if($results) {

            $row = mysqli_fetch_array($results);

            $userId = $row['id'];

            $userPicture = URLdecode($row['picture']);

            $data['code'] = 'success';

            $data['data'] = array('id'=>$userId,'pic'=>$userPicture);

        } else {

            die('Error: ' . mysqli_error($con));

        }

    } else {

        $data['code'] = 'failure';

        $data['msg'] = '用户不存在';

    }

    mysqli_close($con);

    echo json_encode($data);

?>