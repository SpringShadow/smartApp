<?php
/**
 * Created by PhpStorm.
 * User: keke
 * Date: 2020/2/20
 * Time: 13:52
 */

    include('commonFunction.php');

    $cf = new commonFunction();


    $linkId = $cf->test_input($_POST["linkId"]);

    $childIds = $cf->test_input($_POST["childIds"]);


    $data['code'] = 'failure';

    $data['msg'] = '准备删除不同版本图纸';

    $data['data'] = '';

    $count = 0 ;


    // get children
    $children = $cf->getValueByKey('m_children', $con, $_MODEL_TABLE, 'm_id', $linkId);

    if($children != null && $children != ''){

        $childrenArray = json_decode(htmlspecialchars_decode($children), true);

        $childrenNewArray = array();

        // delete all children
        foreach ($childrenArray as $m_id) {

            if (stripos($childIds, $m_id . '') > -1) {

                // delete children by id in sql
                $sql = "DELETE FROM {$_MODEL_TABLE} WHERE m_id = '$m_id'";

                $results = mysqli_query($con, $sql);

                if($results){

                    $count ++;

                }

            } else {

                array_push($childrenNewArray, $m_id);

            }

        }

        $childrenNewString = htmlspecialchars(json_encode($childrenNewArray, JSON_UNESCAPED_UNICODE));


        // update values to sql
        $sql = "UPDATE {$_MODEL_TABLE} SET m_children = '$childrenNewString' WHERE m_id = '$linkId'";

        $results = mysqli_query($con, $sql);

        if($results){

            $data['code'] = 'success';

            $data['msg'] = '删除 ' . $count . ' 个不同版本图纸成功';

            $data['data'] = array(

                'countTxt' => count($childrenNewArray),

            );

        }

    } else {

        $data['msg'] = '没有不同版本图纸';

    }

    mysqli_close($con);

    echo json_encode($data);

?>