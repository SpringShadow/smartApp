<?php
/**
 * Created by PhpStorm.
 * User: keyuhan
 * Date: 2020/1/23
 * Time: 21:27
 */
    include('commonFunction.php');

    $cf = new commonFunction();


    $Authorization = $cf->get_access_token();

    $modelId = $cf->test_input($_POST["modelId"]);

    $fileId = $cf->getValueByKey('m_fileid', $con, $_MODEL_TABLE, 'm_id', $modelId);


    $header = array(

        'Authorization: '.$Authorization,

        'Content-type: application/json'
    );

    $data['code'] = 'failure';

    $data['msg'] = '准备获取浏览权限';

    $data['data'] = array();


    // $viewToken = do_get("https://api.bimface.com/view/token?fileId=".$fileId, $header);

    $viewToken = $cf->curl_http("https://api.bimface.com/view/token?fileId=".$fileId, "GET", $header,null);

    $viewTokenArray = json_decode($viewToken, true);

    if($viewTokenArray['code'] == 'success'){

        $cf->setSelectValueAdd('m_browsetimes', $con, $_MODEL_TABLE, 'm_id', $modelId);

        $data['code'] = 'success';

        $data['msg'] = '获得权限成功';

        $data['data'] = array(

            'viewToken'=>$viewTokenArray['data'],

            'time'=>date('Y-m-d H:i:s'),

        );

    } else {

        $data['msg'] = '没有获得浏览权限';

    }

    echo json_encode($data);
?>