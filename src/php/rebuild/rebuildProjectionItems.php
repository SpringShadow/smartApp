<?php
/**
 * Created by PhpStorm.
 * User: keyuhan
 * Date: 2020/3/21
 * Time: 17:43
 */

include('../commonFunction.php');

$cf = new commonFunction();

$p_id = $cf->test_input($_GET["p_id"]);


$data['code'] = 'failure';

$data['msg'] = '准备重建图纸目录';


$modelIds = $cf->getValuesByKey('m_id',$con,$_MODEL_TABLE,'m_projection', $p_id);

$projectionItems = array();

foreach ($modelIds as $m_id){

    $modelName = $cf->getValueByKey('m_name',$con,$_MODEL_TABLE,'m_id',$m_id);

    $modelIcon = $cf->getValueByKey('m_suffix',$con,$_MODEL_TABLE,'m_id',$m_id);

    $modelStatus = $cf->getValueByKey('m_status',$con,$_MODEL_TABLE,'m_id',$m_id);

    $modelDate = $cf->getValueByKey('m_createtime',$con,$_MODEL_TABLE,'m_id',$m_id);

    if($modelStatus == 'translate_ok'){

        array_push($projectionItems,array(

            'id'=>$m_id,

            'name'=>$modelName,

            'icon'=>$modelIcon,

            'badge_value'=>array(

                'build'=>"1#",

                'countTxt'=>"0",

                'date'=>$modelDate,

                'plan'=>"方案",

                'tech'=>"建筑",

                'topTxt'=>"1# - 建筑 - 方案",

                'version'=>""
                
            )

        ));

    }

}

$projectionItemsStr = htmlspecialchars(json_encode($projectionItems, JSON_UNESCAPED_UNICODE));


// update values to sql
$sql = "UPDATE {$_PROJECTION_TABLE} SET p_items = '$projectionItemsStr' WHERE p_id = '$p_id'";

$results = mysqli_query($con, $sql);

if($results){

    $data['code'] = 'success';

    $data['msg'] = '重建图纸目录成功';

}

$data['data'] = $projectionItemsStr;


mysqli_close($con);

echo json_encode($data);

?>