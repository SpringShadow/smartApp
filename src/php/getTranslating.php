<?php
/**
 * Created by PhpStorm.
 * User: keyuhan
 * Date: 2020/1/26
 * Time: 17:50
 */

    include('commonFunction.php');

    $cf = new commonFunction();


    $Authorization = $cf->get_access_token();

    $fileId = $cf->test_input($_POST["fileId"]);

    $header = array(

        'Authorization: '.$Authorization,

        'Content-type: application/json'
    );

    $translating = $cf->curl_http("https://api.bimface.com/translate?fileId=".$fileId, "GET", $header,null);

    echo $translating;
?>