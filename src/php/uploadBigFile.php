<?php
/**
 * Created by PhpStorm.
 * User: keyuhan
 * Date: 2020/1/26
 * Time: 21:08
 */


    include('commonFunction.php');

    $cf = new commonFunction();


    $p_id = $cf->test_input($_POST["p_id"]);

    $fileData = $cf->test_input($_POST["file"]);

    $baseFileName = $cf->test_input($_POST["baseFileName"]);// file name

    $fileName = $cf->test_input($_POST["fileName"]);// time + file name

    $totalPieces = $cf->test_input($_POST["totalPieces"]);

    $nowPieces = $cf->test_input($_POST["nowPieces"]);

    $fileLength = $cf->test_input($_POST["fileLength"]);

    $fileSuffix = $cf->test_input($_POST["fileSuffix"]);

    $modelId = $cf->test_input($_POST["modelId"]);

    // $userId = "edraw.xyz";


    $data['code'] = 'failure';

    $data['msg'] = '准备上传大文件';

    $data['data'] = '';

    // get file type string
    $fileType = $cf->getFileTypeString($fileSuffix);


    // get creator id in online projection
    $p_creatorId = $cf->getValueByKey('p_creator_id', $con, $_PROJECTION_TABLE, 'p_id', $p_id);

    // projection dir
    $projectionPath = $cf->createDirByUserIdAndProjectionId($p_creatorId, $p_id);


    // upload chunk
    // $new_file_path = $_SERVER['SINASRV_MODELS_DIR'].'/'.$userId.'/'. $fileName.$nowPieces;

    $new_file_path = $projectionPath . '/' . $fileName . $nowPieces;

    if (move_uploaded_file($_FILES['file']['tmp_name'], $new_file_path)) {

        //判断是否上传完成
        $status = checkUploadAccomplish($projectionPath, $fileName, $totalPieces);

        if($status['status']){

            $fileId = 'tuku_f_' . $cf->random(16);// tuku_f_XXXXXXXXXXXXXX

            $targetFileName = $fileId . '.' . $fileSuffix;

            //合并切片文件
            $merge = mergeSlices($projectionPath, $targetFileName, $fileName, $totalPieces);

            // $file_path = str_replace($_SERVER['SINASRV_MODELS_DIR'],$_SERVER['EDRAW_MODELS_DIR'],$file_path);

            //验证文件安全,xxx

            if($merge){

                $now = date('Y-m-d H:i:s');

                $status = 'translate_ok';

                // update timeline and children values to sql
                $sql = "UPDATE {$_MODEL_TABLE} 
                        SET m_status='$status', m_lasttime='$now', m_length='$fileLength', m_suffix='$fileSuffix',m_fileid='$fileId'
                        WHERE m_id='$modelId'";

                $results = mysqli_query($con, $sql);

                if($results){

                    $data['code'] = 'success';

                    $data['msg'] = '上传' . $fileType . '成功';

                    $data['data'] = array(

                        'status'=>$status,

                        'time'=>$now,

                    );

                } else {

                    $data['msg'] = '保存数据库失败';

                }

            } else {

                $data['msg'] = '合并' . $fileType . '失败';

            }

        }

    } else {

        $data['code'] = 'failure';

        $data['msg'] = '上传' . $fileType . '失败';;

    }

    echo json_encode($data);

    /*

    * 判断文件是否上传完成

    * */

    function checkUploadAccomplish($projectionPath, $file_name, $total_pieces)

    {

        $n = 1;

        $status = true;

        for($i=1; $i<=$total_pieces; $i++)

        {

            if(!file_exists($projectionPath .'/'. $file_name . $i))

            {

                $status = false;

                return ['n'=>$n, 'status'=>$status];

            }else{

                $n++;

            }

        }

        return ['n'=>$n, 'status'=>$status];

    }

    /*

    * 合并切片文件

    * */

    function mergeSlices($projectionPath, $targetFileName, $file_name, $total_pieces)

    {

        $new_file_name = $projectionPath . '/' . $targetFileName;

        $fp = fopen($new_file_name,"wb");

        for($i=1; $i<=$total_pieces; $i++)

        {

            $tmp_name = $projectionPath . '/'. $file_name . $i;

            //只读方式打开文件 指针指向文件头

            $handle = fopen($tmp_name,"rb");

            //切片文件写入新文件

            fwrite($fp,fread($handle,filesize($tmp_name)));

            //关闭切片文件

            fclose($handle);

            unset($handle);

            //删除切片

            unlink($tmp_name);

        }

        fclose($fp);

        return true;

    }

?>


