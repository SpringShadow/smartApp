<?php
/**
 * Created by PhpStorm.
 * User: keyuhan
 * Date: 2018/8/20
 * Time: 22:26
 */

/**
 * 微信开放平台获取客户资料（PC（手机）端生产二维码，手机端扫码登录）
 */

header("Content-type: text/html; charset=utf-8");

include("commonFunction.php");

$cf = new commonFunction();

$_ID = "wxcb7237f3ff9cd5d1";

$_SECRET = "bbfc16b9301ee058c2fedb5bd172b9aa";

$_STATE = "kCC0gQaJUPn1I4WwGa1iQupNNVylxIF2";

$code = $cf->test_input($_GET["code"]);

$state = $cf->test_input($_GET["state"]);

$p_id = $cf->test_input($_GET["pid"]);

if ($code && $state == $_STATE) {

    $url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=$_ID&secret=$_SECRET&code=$code&grant_type=authorization_code";

    $access_token_str = file_get_contents($url);
    // echo $access_token_str;

    // get user info
    if ($access_token_str) {

        $access_array = json_decode($access_token_str);

        $access_token = $access_array->access_token;;

        $openid = $access_array->openid;

        $unionId = $access_array->unionid;

        $access_url = "https://api.weixin.qq.com/sns/userinfo?access_token=$access_token&openid=$openid";

        $wx_user_info = file_get_contents($access_url);

        $wx_user_info = str_replace("\\/", "/", $wx_user_info);

        $wx_user_array = json_decode($wx_user_info);

        $nickName = $wx_user_array->nickname;

        $nickName = urlencode($nickName);// change nick

        $headImgUrlStr = $wx_user_array->headimgurl;

        $headImgUrl = urlencode($headImgUrlStr);// change

        $sex = $wx_user_array->sex;

        $country = $wx_user_array->country;

        $province = $wx_user_array->province;

        $city = $wx_user_array->city;

        $from = "EDRAW_ONLINE";

        if (!empty($nickName) && !empty($unionId)) {

            // insert user info to sql and gei use id 
            include("wxWelcomeBehind.php");

            if($p_id != ''){
                
                echo '<script type=text/javascript>top.window.location.href="' . $_LOGIN_CALLBACK_URL . '/projection?id=' . $p_id . '&user=' . $userId . '"</script>';

            } else {

                echo '<script type=text/javascript>top.window.location.href="' . $_LOGIN_CALLBACK_URL . '/user?id=' . $userId . '"</script>';

            }

        }

    }

}
?>