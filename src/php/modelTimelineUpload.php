<?php
/**
 * Created by PhpStorm.
 * User: keke
 * Date: 2020/2/11
 * Time: 23:19
 */

    include('commonFunction.php');

    $cf = new commonFunction();


    $timeline = $cf->test_input($_POST["timeline"]);

    $modelId = $cf->test_input($_POST["modelId"]);


    $data['code'] = 'failure';

    $data['msg'] = '准备上传时间线';

    $data['data'] = array();


    // get merge tile line string
    $mergeTimeline = _getTimeLine($cf, $con, $_MODEL_TABLE, $modelId, $timeline);

    // update values to sql
    $sql = "UPDATE {$_MODEL_TABLE} SET m_createline = '$mergeTimeline' WHERE m_id = '$modelId'";

    $results = mysqli_query($con, $sql);

    if($results){

        $data['code'] = 'success';

        $data['msg'] = '上传时间线完成';

    }

    mysqli_close($con);

    echo json_encode($data);



    function _getTimeLine($cf, $con, $_MODEL_TABLE, $modelId, $inputStr){

        // get string value

        $str = $cf->getValueByKey('m_createline', $con, $_MODEL_TABLE, 'm_id', $modelId);

        // get array

        if($str != null && $str != ''){

            $array = json_decode(htmlspecialchars_decode($str), true);

        } else {

            $array = array();

        }

        // get add time line array

        $inputArray = json_decode(htmlspecialchars_decode($inputStr), true);

        // merge array

        $array = array_merge($array, $inputArray);

        // change to str

        $newStr = htmlspecialchars(json_encode($array, JSON_UNESCAPED_UNICODE));

        return $newStr;

    }

?>