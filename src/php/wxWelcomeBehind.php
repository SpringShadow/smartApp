<?php
/**
 * Created by PhpStorm.
 * User: keyuhan
 * Date: 2018/7/29
 * Time: 3:19
 */

// insert user info to sql
if (!empty($nickName)) {

    // 查找unionid(用户唯一编码来自微信)
    $sql = "SELECT * FROM online_user WHERE unionid = '$unionId'";

    $results = mysqli_query($con, $sql);

    // 当前时间
    $nowTime = date('Y-m-d H:i:s');

    // 如果unionid不存在
    if ($results->num_rows < 1) {               //num_rows == 0 ，

        // 向数据库表插入数据?
        // 根据用户数量，确定注册用户的序号

        // get user login number
        $sum = $cf->setSelectValueAdd('USERLOGINNUMBER', $con, 'zimodel_info', 'INFO', 'EDRAW_ONLINE');

        // 自动用户序号
        // $userId = num2str($sum, 7);
        $userId = 'tuku_'.$sum.'O'.$cf->random(12);

        // 默认密码
        $password = "";// 微信用户没有密码

        // 注册来源
        $loginFrom = $from . "/" . "WeiXin/" . $cf->getDevice();

        // 插入数据
        $sql = "INSERT INTO online_user (username, email, password, picture , id , unionid , logintime , lasttime, loginfrom,sex,country,province,city) VALUES ('$nickName','$email','$password','$headImgUrl','$userId','$unionId','$nowTime','$nowTime','$loginFrom','$sex','$country','$province','$city')";

        if (!mysqli_query($con, $sql)) {

            die('Error: ' . mysqli_connect_error());

            $welcomeStr = "注册错误";

        } else {

            $welcomeStr = "欢迎注册EDRAW.XYZ";

        }

        // create user's direction
        createUserDirection($userId);

    } else {                                    //username is existed.

        // get user id of edraw.xyz
        $row = mysqli_fetch_array($results);

        $userId = $row['id'];

        // update user name and picture (maybe changed)

        $cf->updateSelectValue('username',$nickName, $con, 'online_user', 'unionid', $unionId);

        $cf->updateSelectValue('picture',$headImgUrl, $con, 'online_user', 'unionid', $unionId);

        // welcome text

        $welcomeStr = "欢迎登录EDRAW.XYZ";

        // update login times
        $cf->setSelectValueAdd('logintimes', $con, 'online_user', 'unionid', $unionId);

        // update logintime
        $cf->updateSelectValue('lasttime', $nowTime, $con, 'online_user', 'unionid', $unionId);

    }

    // 关闭连接
    mysqli_close($con);

    echo $welcomeStr.'<br>';

}

//将数字转化字符串并指定字符串长度，长度不够左侧补0。
function num2str($num, $length)
{

    $num_str = (string)$num;

    $num_strlength = count($num_str);

    if ($length > $num_strlength) {

        $num_str = str_pad($num_str, $length, "0", STR_PAD_LEFT);

    }

    return $num_str;

}

//create user's direction
function createUserDirection($id)
{

    $userpath = $_SERVER['SINASRV_USERS_DIR'].'/' . $id;

    //echo "目录 $userpath 创建";
    if (!is_dir($userpath)) {

        $res = mkdir($userpath, 0777, true);

        if ($res){

            $create = "$userpath 创建成功";

        }else{

            $create = "$userpath 创建失败";

        }

    } else {

        $create = "$userpath 已经存在！";

    }

    return $create;
}