<?php
/**
 * Created by PhpStorm.
 * User: keyuhan
 * Date: 2017/11/16
 * Time: 23:48
 */

$_SERVER['IS_TEST_MODE'] = stripos($_SERVER['REQUEST_URI'], '/test_php/') > 0;

$_WEB_KEY = $_SERVER['IS_TEST_MODE']? 'online' : 'yun';

$_MODEL_TABLE = $_SERVER['IS_TEST_MODE'] ? '_test_model' : 'online_model';

$_PROJECTION_TABLE = $_SERVER['IS_TEST_MODE'] ? '_test_projection' : 'online_projection';

$_SERVER['MODEL_CALLBACK_URL'] = $_SERVER['IS_TEST_MODE'] ? 'https://www.edraw.xyz/' . $_WEB_KEY . '/test_php/' : 'https://www.edraw.xyz/' . $_WEB_KEY . '/php/';

$_SERVER['SINASRV_MODELS_DIR'] = '/data/home/hyu5667950001/htdocs/'.$_WEB_KEY.'/models';

$_SERVER['EDRAW_MODELS_DIR'] = 'https://www.edraw.xyz/'.$_WEB_KEY.'/models';

$_SERVER['SINASRV_USERS_DIR'] = '/data/home/hyu5667950001/htdocs/'.$_WEB_KEY.'/users';

$_SERVER['EDRAW_USERS_DIR'] = 'https://www.edraw.xyz/'.$_WEB_KEY.'/users';

$_SERVER['EDRAW_IMAGE_DIR'] = 'https://www.edraw.xyz/'.$_WEB_KEY.'/image';

// $_LOGIN_CALLBACK_URL = $_SERVER['IS_TEST_MODE'] ? 'http://localhost:1111/#' : 'https://www.edraw.xyz/test/tuku/index.html#';

// $_LOGIN_CALLBACK_URL = 'https://www.edraw.xyz/test/tk/index.html#';

$_LOGIN_CALLBACK_URL = 'https://www.edraw.xyz/' . $_WEB_KEY . '/index.html#';

include("connectMsql.php");

class commonFunction
{

    //------------------------------------------------------function----------------------------------------------------
    // optimization input string
    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        //$data = iconv('UTF-8','GBK',$data);
        return $data;
    }

    // get a value by selectKey from $table by $indexKey = $indexValue
    function getValueByKey($selectKey, $con, $table, $indexKey, $indexValue)
    {
        $results = mysqli_query($con, "SELECT * FROM " . $table . " WHERE " . $indexKey . " = '$indexValue'");
        // echo "SELECT * FROM " . $table . " WHERE " . $indexKey . " = '$indexValue'";
        $row = mysqli_fetch_array($results);
        $value = $row[$selectKey];

        /*$fields = mysqli_fetch_fields($results);              //列出所有的name
        while($row = mysqli_fetch_row($results)) {              //每一行
            $i = 0;
            foreach ($fields as $val){
                if($val->name == $selectKey){
                    $value = $row[$i];
                    break;
                }
                $i++;
            }
        }*/

        // mysqli_close($con);

        return $value;
    }

    function print_row($results){

        $fields = mysqli_fetch_fields($results);                //列出所有的

        while ($row = mysqli_fetch_row($results)) {             //每一行

            $i = 0;

            foreach ($fields as $val) {

                echo $val->name .':' .$row[$i] . '<br>';

                $i++;
            }
        }

    }

    // get values(array) by selectKey from $table by $indexKey = $indexValue
    function getValuesByKey($selectKey, $con, $table, $indexKey, $indexValue)
    {
        $results = mysqli_query($con, "SELECT * FROM " . $table . " WHERE " . $indexKey . " = '$indexValue'");
        // echo "SELECT * FROM " . $table . " WHERE " . $indexKey . " = '$indexValue'";
        /*$row = mysqli_fetch_array($results);
        $value = $row[$selectKey];*/

        $values = array();
        $fields = mysqli_fetch_fields($results);                //列出所有的
        while ($row = mysqli_fetch_row($results)) {             //每一行
            $i = 0;
            foreach ($fields as $val) {
                if ($val->name == $selectKey) {
                    $value = $row[$i];
                    array_push($values, $value);
                }
                $i++;
            }
        }

        // mysqli_close($con);

        return $values;
    }

    // set random string
    function random($length)
    {
        $chars = '123456789abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ';
        $hash = '';
        $max = strlen($chars) - 1;
        for ($i = 0; $i < $length; $i++) {
            $hash .= $chars[mt_rand(0, $max)];
        }
        return $hash;
    }

    // set random file name
    function randomFullName($length, $FullName)
    {
        $chars = '123456789abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ';
        $hash = '';
        $max = strlen($chars) - 1;
        for ($i = 0; $i < $length; $i++) {
            $hash .= $chars[mt_rand(0, $max)];
        }

        $extStr = explode('.', $FullName);
        $count = count($extStr) - 1;
        $ext = $extStr[$count];

        return $hash . '.' . $ext;
    }

    // delete component image file
    function deleteImageFile($con, $cindex)
    {
        // get image file name
        $imageName = $this->getValueByKey("images", $con, "zimodel_components", "cindex", $cindex);
        $imageFile = '/data/home/hyu5667950001/htdocs/main/image/templates/component/' . $imageName;
        //delete file
        if (is_file($imageFile)) {
            $delete = unlink($imageFile) or die("Unable to find file!");
        }
        /*if($delete){
            echo 'success delete image of component ' . $cindex;
            echo '<br />';
        }*/
    }

    // add children index to children's value of templates
    function addTemplateChildren($con, $parent, $childrenIndex)
    {
        // get children of template
        $sql = "SELECT * FROM zimodel_templates WHERE qindex = '$parent'";
        $results = mysqli_query($con, $sql);
        $row = mysqli_fetch_array($results);
        $oldStr = $row['children'];
        /*$fields = mysqli_fetch_fields($results);             //列出所有的name
        while($row = mysqli_fetch_row($results)) {           //每一行
            $i=0;
            foreach ($fields as $val){
                if($val->name == 'children'){
                    $oldStr = $row[$i];
                    break;
                }
                $i++;
            }
        }*/
        // add component index
        if (!$oldStr) {
            $newStr = $childrenIndex;
        } else {
            $newStr = $oldStr . ',' . $childrenIndex;
        }
        $sortStr = $this->sortTemplateChildrenByType($con, $newStr);
        $sql = "UPDATE zimodel_templates SET children = '$sortStr' WHERE qindex = '$parent'";
        mysqli_query($con, $sql);
    }

    // update(sort) children's value of template
    function updateTemplateChildren($con, $parent)
    {
        $sql = "SELECT * FROM zimodel_templates WHERE qindex = '$parent'";
        $results = mysqli_query($con, $sql);
        $row = mysqli_fetch_array($results);
        $childrenStr = $row['children'];
        // echo "children:" . $childrenStr;
        $sortStr = $this->sortTemplateChildrenByType($con, $childrenStr);
        $sql = "UPDATE zimodel_templates SET children = '$sortStr' WHERE qindex = '$parent'";
        mysqli_query($con, $sql);
    }

    // sort children strings of template
    function sortTemplateChildrenByType($con, $childrenStr)
    {
        // sort by component's ctype
        $strArray = explode(',', $childrenStr);
        // print_r($strArray);
        $array = array();
        foreach ($strArray as $childrenIndex) {
            $childrenType = $this->getValueByKey('ctype', $con, 'zimodel_components', 'cindex', $childrenIndex);
            if (!$array[$childrenType]) {
                $array[$childrenType] = array();
            }
            array_push($array[$childrenType], $childrenIndex);
        }

        $sortKeys = array('group', 'layoutGroup', 'layoutVThree', 'layoutUp', 'layoutMiddle', 'layoutBottom', 'wall', 'window', 'door', 'skp');
        $sortIndexs = array();
        foreach ($sortKeys as $key) {
            if ($array[$key]) {
                $sortIndexs = array_merge($sortIndexs, $array[$key]);
            }
        }
        return implode(",", $sortIndexs);
    }

    // down times add
    function downTimesAdd($con, $table, $indexKey, $indexValue)
    {
        // get down times
        $results = mysqli_query($con, "SELECT * FROM " . $table . " WHERE " . $indexKey . " = '$indexValue'");
        $row = mysqli_fetch_array($results);
        $value = $row['downtimes'] + 1;
        // update down times
        mysqli_query($con, "UPDATE $table SET downtimes = '$value' WHERE " . $indexKey . " = '$indexValue'");
    }

    // userLoginNumber no auto add
    function setSelectValueAdd($selectKey, $con, $table, $indexKey, $indexValue)
    {
        // get *userLoginNumber*
        $results = mysqli_query($con, "SELECT * FROM {$table} WHERE {$indexKey} = '$indexValue'");
        $row = mysqli_fetch_array($results);
        $value = $row[$selectKey] + 1;
        // update *userLoginNumber*
        mysqli_query($con, "UPDATE {$table} SET {$selectKey} = '$value' WHERE {$indexKey} = '$indexValue'");
        return $value;
    }

    function updateSelectValue($selectKey, $selectValue, $con, $table, $indexKey, $indexValue)
    {
        // update $selectKey
        $results = mysqli_query($con, "UPDATE {$table} SET {$selectKey} = '$selectValue' WHERE {$indexKey} = '$indexValue'");
        return $results;
    }

    function getDevice()
    {
        $agent = $this->getUA();
        if ($agent != false) {
            if (true == preg_match("/.+Windows.+/", $agent)) {
                return "WIN";
            } elseif (true == preg_match("/.+Macintosh.+/", $agent)) {
                return "MAC";
            } elseif (true == preg_match("/.+iPad.+/", $agent)) {
                return "IPAD";
            } elseif (true == preg_match("/.+iPhone.+/", $agent)) {
                return "IPHONE";
            } elseif (true == preg_match("/.+Android.+/", $agent)) {
                return "ANDROID";
            }
        }
        return "unknown";
    }

    function getUA()
    {
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            return $_SERVER['HTTP_USER_AGENT'];
        } else {
            return false;
        }

    }

    function createFolder($filePath)
    {
        $dir = iconv("UTF-8", "GBK", $filePath);//文件夹路径

        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
    }

    /*
     * php访问url路径，get请求
     * $url:路径url
     * $headers:array()   post参数数据
     */
    function do_get($url, $headers)
    {
        // 初始化
        $curl = curl_init();
        // 设置url路径
        curl_setopt($curl, CURLOPT_URL, $url);
        // 将 curl_exec()获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // 在启用 CURLOPT_RETURNTRANSFER 时候将获取数据返回
        curl_setopt($curl, CURLOPT_BINARYTRANSFER, true);
        // 添加头信息
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        // CURLINFO_HEADER_OUT选项可以拿到请求头信息
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        // 不验证SSL
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        // 执行
        $data = curl_exec($curl);
        // 关闭连接
        curl_close($curl);
        // 返回数据
        return $data;
    }


    /*
     * php访问url路径，post请求
     * $url:路径url
     * $headers:array()   post参数数据
     */

    function do_post($url, $headers, $params)
    {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_TIMEOUT, 60);

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;

    }

    /*
     *   php访问url路径，post请求
     *   durl:路径url
     *   $headers:array()   post参数数据
     */
    function curl_file_post_contents($durl, $headers)
    {
        // header传送格式
        //        $headers = array(
        //            "authorization:Basic NUh4bnNwTkFvMHZPeGJxbDVYTUo0Zzdvd2FyQWhja2k6SnExaWdCNjRLc24weFRndXRVek9OdUNWQ2ZZdXZld2Y=",
        //        );
        //初始化
        $curl = curl_init();
        //设置抓取的url
        curl_setopt($curl, CURLOPT_URL, $durl);
        //设置头文件的信息作为数据流输出
        curl_setopt($curl, CURLOPT_HEADER, false);
        //设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //设置post方式提交
        curl_setopt($curl, CURLOPT_POST, true);
        // 设置post请求参数
        // curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
        // 添加头信息
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        // CURLINFO_HEADER_OUT选项可以拿到请求头信息
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        // 不验证SSL
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        //执行命令
        $data = curl_exec($curl);
        // 打印请求头信息
        // echo curl_getinfo($curl, CURLINFO_HEADER_OUT);
        //关闭URL请求
        curl_close($curl);
        //显示获得的数据
        return $data;
    }


    function curl_http($url, $type, $headers, $params)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($curl, CURLOPT_TIMEOUT, 60);

        if ($headers != "") {

            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        } else {

            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));

        }

        switch ($type) {

            case "GET" :

                curl_setopt($curl, CURLOPT_HTTPGET, true);

                break;

            case "POST":

                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");

                curl_setopt($curl, CURLOPT_POSTFIELDS, $params);

                break;

            case "PUT" :

                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");

                curl_setopt($curl, CURLOPT_POSTFIELDS, $params);

                break;

            case "PATCH":

                curl_setopt($curl, CULROPT_CUSTOMREQUEST, 'PATCH');

                curl_setopt($curl, CURLOPT_POSTFIELDS, $params);

                break;

            case "DELETE":

                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");

                curl_setopt($curl, CURLOPT_POSTFIELDS, $params);

                break;
        }

        // 不验证SSL
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);

        $result = curl_exec($curl);//获得返回值

        curl_close($curl);

        return $result;
    }

    /**
     * @return array{appKey , appSecret}
     */
    function _getAppKeyAndAppSecret(){

        if($_SERVER['IS_TEST_MODE']){

            return array(// ->56772415

                'appKey'=>'5HxnspNAo0vOxbql5XMJ4g7owarAhcki',

                'appSecret'=>'Jq1igB64Ksn0xTgutUzONuCVCfYuvewf'

            );

        }

        else {

            return array(// ->57100690

                'appKey'=>'NIiOLVd8DECuhpCq0zXfXrkI0UsL7vI0',

                'appSecret'=>'P91ZExvUYFdbfEHfJVey9u7kOn4Kzna4'

            );

        }

    }

    // get signature on md5
    function getSignature($fileId,$status,$nonce)
    {
        $appArray = $this->_getAppKeyAndAppSecret();

        $appKey = $appArray['appKey'];

        $appSecret = $appArray['appSecret'];

        return md5($appKey.':'.$appSecret.':'.$fileId.':'.$status.':'.$nonce);

    }




    // 将字符串 appKey:appSecret 拼接后（中间用冒号连接），对其进行BASE64编码，
    // 然后在编码后的字符串前添加字符串Basic和一个空格， 即：“Basic” + “ ” + Base64Encode(appKey + “:” + appSecret)
    function get_authorization()
    {
        // get app key and secret
        $appArray = $this->_getAppKeyAndAppSecret();

        $appKey = $appArray['appKey'];

        $appSecret = $appArray['appSecret'];


        // get encodeStr
        $appStr = $appKey . ":" . $appSecret;

        $encodeStr = base64_encode($appStr);

        return ("Basic " . $encodeStr);
    }

    // get access token of bim face by Authorization
    function get_access_token(){

        $header = array(

            'Authorization: '.$this->get_authorization(),

            'Content-Type: application/json'
        );

        $accessToken = $this->curl_http("https://api.bimface.com/oauth2/token", "POST", $header,null);

        $resultArray = json_decode($accessToken, true);

        if($resultArray['code'] == "success") {

            return 'Bearer ' . $resultArray['data']['token'];

        } else {

            return 'failure';
        }

    }

    /**
     * @param $arr
     * @param $timeKeyStr
     * @param $isSortASC: true or false , SORT_ASC 升 ， SORT_DESC 降
     * @return mixed
     */
    function sortArrayByTime($arr, $timeKeyStr, $isSortASC){

        $ctime_str = array();

        foreach($arr as $key=>$v){

            $arr[$key][$timeKeyStr] = date("Y-m-d H:i:s", strtotime($v[$timeKeyStr]));

            $ctime_str[] = $arr[$key][$timeKeyStr];

        }

        $isSortASC ? array_multisort($ctime_str, SORT_ASC, $arr) : array_multisort($ctime_str, SORT_DESC, $arr);

        return $arr;

    }

    /**add addValue to msql save in string
     * @param $con
     * @param $selKey
     * @param $table
     * @param $tableKey
     * @param $tableValue
     * @param $addValue
     * @param $isUpdateTable
     * @return bool|mysqli_result
     */
    function pushToArray($con, $selKey, $table, $tableKey, $tableValue, $addValue, $isUpdateTable){

        // get string value

        $str = $this->getValueByKey($selKey, $con, $table, $tableKey, $tableValue);

        // get array

        if($str != null && $str != ''){

            $array = json_decode(htmlspecialchars_decode($str), true);

        } else {

            $array = array();

        }

        // push to array

        array_push($array, $addValue);

        // change to str

        $newStr = htmlspecialchars(json_encode($array, JSON_UNESCAPED_UNICODE));

        // whether is update ?

        if($isUpdateTable){

            // update value to sql
            $sql = "UPDATE {$table} SET {$selKey}='$newStr' WHERE {$tableKey} = '$tableValue'";

            $results = mysqli_query($con, $sql);

            return $results;

        } else {

            return $newStr;

        }

    }


    /**save projection to user
     * @param $con
     * @param $_PROJECTION_TABLE
     * @param $projectionId
     * @param $userId
     * @return bool|mysqli_result
     */
    function saveProjectionIdToUser($con, $_PROJECTION_TABLE, $projectionId, $userId){

        $userProjection = $this->getValueByKey('projection', $con, 'online_user', 'id', $userId);

        if($userProjection != null && $userProjection != ''){

            // get array
            $userProjectionArray = json_decode(htmlspecialchars_decode($userProjection), true);

        } else {

            $userProjectionArray = array();

        }

        // whether is p id is exist!

        $userProjectionNewArray = $userProjectionArray;

        /*$userProjectionNewArray = array();

        foreach ($userProjectionArray as $p_id){

            $sql = "SELECT * {$_PROJECTION_TABLE} WHERE p_id = '$p_id'";

            $results = mysqli_query($con, $sql);

            // if not exist to delete from array
            if($results){

                array_push($userProjectionNewArray, $p_id);

            }

        }*/

        array_push($userProjectionNewArray, $projectionId);

        $userProjectionNewArray = array_unique($userProjectionNewArray);

        $userProjectionStr = htmlspecialchars(json_encode($userProjectionNewArray, JSON_UNESCAPED_UNICODE));

        // update value to sql
        $sql = "UPDATE online_user SET projection='$userProjectionStr' WHERE id = '$userId'";

        $results = mysqli_query($con, $sql);

        return $results;

    }

    /**create projection dir (name = projection id) under user dir(name = user id)
     * @param $p_creatorId
     * @param $projection_id
     * @return string
     */
    function createDirByUserIdAndProjectionId($p_creatorId, $projection_id){

        $userPath =  $_SERVER['SINASRV_USERS_DIR'] . '/' . $p_creatorId;

        if(!is_dir($userPath)){

            mkdir($userPath);// 创建 目录

        }

        $projectionPath = $userPath . '/' .$projection_id;

        if(!is_dir($projectionPath)){

            mkdir($projectionPath);// 创建 目录

        }

        return $projectionPath;

    }

    function saveImageToPath($userPath, $fileImage, $fileName){

        if ($fileImage) {

            // $fileImage = str_replace('data:image/png;base64,', '', $fileImage);
            $fileImage = str_replace('data:image/jpeg;base64,', '', $fileImage);

            $fileImage = str_replace(' ', '+', $fileImage);

            if(!$fileName){

                $fileName = random(20) . '.png';

            }

            $filePath = $userPath . "/" . $fileName;

            file_put_contents($filePath, base64_decode($fileImage));

            return true;

        }

        return false;

    }

    /**get projection time line whit create line and events line
     * @param $row
     * @return array
     */
    function getProjectionTimeLine($con, $table, $projectionId, $eventsArray){

        $sql = "SELECT * FROM {$table} WHERE p_id = '$projectionId'";

        $results = mysqli_query($con, $sql);

        $row = mysqli_fetch_array($results);

        $timeline = array(

            array(

                'start' => $row['p_logintime'],

                'end' => '',

                'name' => '创建项目',

                'color'=>'#9e9e9e',//pink

                'creator_id'=> $row['p_creator_id'],

                'creator_imgUrl'=> '',

                'creator'=> '应用',

                'id'=>"tuku_e_0001",

                'type'=>"应用",

            ),

            array(

                'start' => $row['p_lasttime'],

                'end' => '',

                'name' => '最近访问',

                'color'=>'#BDBDBD',

                'creator_id'=> $row['p_creator_id'],

                'creator_imgUrl'=> '',

                'creator'=> '应用',

                'id'=>"tuku_e_0002",

                'type'=>"应用",

            ),

            array(

                'start' => $row['p_date'],

                'end' => $row['p_lasttime'],

                'name' => '共访问 ' . $row['p_browsetimes'] . ' 次',

                'color'=>'#9e9e9e',

                'creator_id'=> $row['p_creator_id'],

                'creator_imgUrl'=> '',

                'creator'=> '应用',

                'id'=>"tuku_e_0003",

                'type'=>"应用",

            ),

        );

        // get projection events

        if($eventsArray == null){

            // get events
            $eventsArray = json_decode(htmlspecialchars_decode($row['p_events']), true);

        }

        if(count($eventsArray) > 0){

            foreach ($eventsArray as $event) {

                $event['creator_imgUrl'] = URLdecode($this->getValueByKey('picture',$con,'online_user','id',$event['creator_id']));

                $event['creator'] = URLdecode($this->getValueByKey('username',$con,'online_user','id',$event['creator_id']));

                array_push($timeline, $event);

            }

        }

        return $timeline;

    }

    function getFileTypeString($fileSuffix){

        $fileType = $fileSuffix == 'dwg'?'图纸':

            $fileSuffix == 'skp' || $fileSuffix == 'rvt'?'3D图纸':

                $fileSuffix == 'pdf'?'pdf文件':

                    stripos('.jpg, .jpeg, .gif, .bmp, .png', $fileSuffix) > -1?'图片':'文件';

        return $fileType;

    }

    /**get file name and extension
     * @param $fileName
     * @return array:{name,extension}
     */
    function getFileNameSuffix($fileName){

        $name = substr($fileName, 0, strrpos($fileName, '.'));

        // $suffix = substr($fileName, strrpos($fileName, '.') + 1);

        return array(

            'name'=>$name,

            'extension'=>$suffix,

        );

    }

    // whether is a model file or image(pdf) file
    function isModelFile($fileSuffix){

        return stripos('.pdf, .jpg, .jpeg, .gif, .bmp, .png', $fileSuffix) > -1 ? false : true;

    }

    /**get file full name when down file
     * @param $con
     * @param $table
     * @param $modelId
     * @param $baseModelName, parent model name
     * @return string: name-[ver]-[time].suffix
     */
    function getFileDownName($con, $table, $modelId, $baseModelName){

        $sql = "SELECT * FROM {$table} WHERE m_id = '$modelId'";

        $results = mysqli_query($con, $sql);

        $row = mysqli_fetch_array($results);

        // $fileName = $this->getFileNameSuffix($row['m_name'])['name'];
        $fileName = $this->getFileNameSuffix($baseModelName)['name'];

        $fileSuffix = $row['m_suffix'];

        $createTime = date("Y-m-d", strtotime($row['m_createtime']));


        $a = array("1" => 'm_build', "2" => 'm_tech', "3" => 'm_plan', "4" => 'm_version');

        $downName = $fileName . '[';

        foreach($a as $v){

            $s = $row[$v];

            if($s != '' && $s != null){

                $downName .= $s . '-';

            }

        }

        $downName .= $createTime . '].' . $fileSuffix;

        return urlencode($downName);

    }

    /**get members' name and picture
     * @param $con
     * @param $p_memberIdsStr
     * @return array
     */
    function getProjectionMemberNamesPics($con, $table, $p_id){

        $p_memberIdsStr = $this->getValueByKey('p_members', $con, $table , 'p_id', $p_id );

        $p_creator_id = $this->getValueByKey('p_creator_id', $con, $table , 'p_id', $p_id );

        $p_memberIds = json_decode(htmlspecialchars_decode($p_memberIdsStr), true);

        $p_memberNamesPics = array();

        foreach ($p_memberIds as $member_id) {

            $member_name = $this->getValueByKey('username', $con, 'online_user', 'id', $member_id);

            $member_pic = $this->getValueByKey('picture', $con, 'online_user', 'id', $member_id);

            if(!empty($member_name)){

                array_push($p_memberNamesPics, array(

                    'name'=>URLdecode($member_name),

                    'picture'=>URLdecode($member_pic),

                    'badge' => $p_creator_id == $member_id ? 'creator' : 'null',

                ));

            }

        }

        return $p_memberNamesPics;

    }

}

?>