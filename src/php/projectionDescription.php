<?php
/**
 * Created by PhpStorm.
 * User: keyuhan
 * Date: 2020/3/8
 * Time: 21:46
 */

include('commonFunction.php');

$cf = new commonFunction();

$p_id = $cf->test_input($_POST["p_id"]);

$p_description = $cf->test_input($_POST["description"]);


// response data

$data['code'] = 'failure';

$data['msg'] = '准备获取项目信息';

$data['data'] = array();


// whether is update or get

if(empty($p_description)){

    // get

    // get events
    $description = $cf->getValueByKey('p_description', $con, $_PROJECTION_TABLE, 'p_id', $p_id);


    $data['code'] = 'success';

    $data['msg'] = '获取项目信息成功';

    $data['data'] = array(

        'description' => htmlspecialchars_decode($description),

    );

} else {

    // update values

    $sql = "UPDATE {$_PROJECTION_TABLE} SET p_description='$p_description' WHERE p_id = '$p_id'";

    $results = mysqli_query($con, $sql);

    if($results) {

        $data['code'] = 'success';

        $data['msg'] = '更新项目信息成功';

        $data['data'] = array(

            'description' =>$p_description

        );

    }

}

mysqli_close($con);

echo json_encode($data);

?>