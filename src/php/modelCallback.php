<?php
/**
 * Created by PhpStorm.
 * User: keke
 * Date: 2020/2/5
 * Time: 20:32
 */

    include('commonFunction.php');

    $cf = new commonFunction();

    $signature = $cf->test_input($_GET["signature"]);

    $fileId = $cf->test_input($_GET["fileId"]);

    $status = $cf->test_input($_GET["status"]);

    $reason = $cf->test_input($_GET["reason"]);

    $nonce = $cf->test_input($_GET["nonce"]);

    $md5 = $cf->getSignature($fileId, $status, $nonce);


    // whether is same signature 
    if($signature == $md5){

        // update status by file id

        $status = 'translate_ok';
        
        // save to create line

        $createLineSuccess = array(

            'title'=>'回调转换成功',

            'content'=>date('Y-m-d H:i:s'),

        );
        
        $createLineStr = $cf->pushToArray($con, 'm_createline', $_MODEL_TABLE, 'm_fileid', $fileId, $createLineSuccess, false);

        // update msql

        $sql = "UPDATE {$_MODEL_TABLE} SET m_status='$status' , m_createline='$createLineStr'

                WHERE m_fileid = '$fileId'";

        $results = mysqli_query($con, $sql);

        mysqli_close($con);

    }

    echo 'HTTP STATUS 200';

?>
