<?php
/**
 * Created by PhpStorm.
 * User: keke
 * Date: 2020/2/6
 * Time: 21:22
 */   

    include('commonFunction.php');
    
    $cf = new commonFunction();

    $projectionId = $cf->test_input($_POST["projectionId"]);


    $data['code'] = 'failure';
    
    $data['msg'] = '查询目录';

    $data['data'] = '';

    
    $sql = "SELECT * FROM {$_PROJECTION_TABLE} WHERE p_id = '$projectionId'";
    
    $results = mysqli_query($con, $sql);

    if($results){

        $row = mysqli_fetch_array($results);

        $cover = $row['p_cover'] != null?

            $_SERVER['EDRAW_USERS_DIR'] . '/' . $row['p_creator_id'] . '/' . $row['p_id'] . '/' . $row['p_cover']:

            $_SERVER['EDRAW_IMAGE_DIR'] . '/icon/project_background.png';

        // get dir type

        $p_dirtype = '';

        if(!empty($row['p_option'])){

            $p_option = json_decode(htmlspecialchars_decode($row['p_option']), true);

            $p_dirtype = empty($p_option['dirtype']) ? "" : $p_option['dirtype'];

        }

        // get projection's timeline

        $timelineArray = $cf->getProjectionTimeLine($con, $_PROJECTION_TABLE, $projectionId, null);

        // get projection's models' items

        $projectionItems = $row['p_items'];

        // get modes' creating

        $modelIds = $cf->getValuesByKey('m_id',$con,$_MODEL_TABLE,'m_projection', $projectionId);

        foreach ($modelIds as $m_id){

            // model id must inside projection's items !!!

            if(true || strpos($projectionItems, $m_id) > -1){

                $m_sql = "SELECT * FROM {$_MODEL_TABLE} WHERE m_id = '$m_id'";

                $m_row = mysqli_fetch_array(mysqli_query($con, $m_sql));

                $modelName = $m_row['m_name'];

                $modelCreatorId = $m_row['m_creator_id'];

                $modelStatus = $m_row['m_status'];

                $modelDate = $m_row['m_logintime'];

                $linkId = $m_row['m_link'];

                // get model's description

                $a = array("1" => 'm_build', "2" => 'm_tech', "3" => 'm_plan', "4" => 'm_version');

                $b = array();

                foreach($a as $v){

                    $s = $m_row[$v];

                    if($s != '' && $s != null){

                        array_push($b, $s);

                    }

                }

                $description = join(' - ', $b);

                if(!empty($modelStatus)){

                    $loading = $modelStatus != 'translate_ok' && $modelStatus != 'update';

                    array_push($timelineArray,array(

                        'is_loading' => $loading,

                        'start' => $modelDate,

                        'end' => '',

                        'name' => $modelName,

                        'pre_text' => $modelStatus == 'begin' ? '上传中-' : $loading ? '转换中-' : '上传',

                        'color'=>'#BDBDBD',

                        'creator_id'=> $modelCreatorId,

                        'creator_imgUrl' => URLdecode($cf->getValueByKey('picture', $con, 'online_user', 'id', $modelCreatorId)),

                        'creator' => URLdecode($cf->getValueByKey('username', $con, 'online_user', 'id', $modelCreatorId)),

                        'id'=>"tuku_e_" . $cf->random(4),

                        'modelId'=> $m_id,

                        'linkId' => empty($linkId) ? '' : $linkId,

                        'description' => $description,

                        'type'=>"应用",

                    ));

                }

            }

        }


        $data['code'] = 'success';

        $data['data'] = array(

            'projection' => array(

                'name' => $row['p_name'],

                'location' => $row['p_location'],

                'date' => $row['p_date'],

                'area' => $row['p_area'],

                'type' => $row['p_type'],

                'creator' =>$row['p_creator'],

                'creator_id' =>$row['p_creator_id'],

                'id' => $row['p_id'],

            ),

            'items' => htmlspecialchars_decode($row['p_items']),

            'members' => $cf->getProjectionMemberNamesPics($con, $_PROJECTION_TABLE, $projectionId),

            'events' => $events,

            'timeline' => $timelineArray,

            'cover' => $cover,//$row['p_cover'],

            'description'=>htmlspecialchars_decode($row['p_description']),

            'dirtype'=>$p_dirtype,

        );

        // browse times ++
        $cf->setSelectValueAdd('p_browsetimes', $con, $_PROJECTION_TABLE, 'p_id', $projectionId);

        // update lasttime by now
        $cf->updateSelectValue('p_lasttime', date('Y-m-d H:i:s'), $con, $_PROJECTION_TABLE, 'p_id', $projectionId);

    }

    mysqli_close($con);

    echo json_encode($data);

?>