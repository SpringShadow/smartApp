<?php
/**
 * Created by PhpStorm.
 * User: keke
 * Date: 2020/2/10
 * Time: 21:14
 */
    include('commonFunction.php');

    $cf = new commonFunction();


    $p_id = $cf->test_input($_POST["p_id"]);

    // $p_creatorId = $cf->test_input($_POST["p_creator_id"]);

    $jpgData = $cf->test_input($_POST["file"]);

    // response data

    $data['code'] = 'failure';

    $data['msg'] = '上传图片';

    $data['data'] = array();


    // get creator id in tuku projection
    $p_creatorId = $cf->getValueByKey('p_creator_id', $con, $_PROJECTION_TABLE, 'p_id', $p_id);

    // projection dir
    $projectionPath = $cf->createDirByUserIdAndProjectionId($p_creatorId, $p_id);

    // upload png to dir , name: cover.png
    $coverName = 'cover.jpg';

    $results1 = $cf->saveImageToPath($projectionPath, $jpgData, $coverName);

    if($results1) {

        $data['msg'] = 'upload image is success';

    }

    // save cover jpg to online_projection
    $results2 = $cf->updateSelectValue('p_cover', $coverName, $con, $_PROJECTION_TABLE, 'p_id', $p_id);

    if($results2) {

        $data['msg'] = 'upload image and save to msql is success ';

    }


    if($results1 && $results2){

        $data['code'] = 'success';

    }

    mysqli_close($con);

    echo json_encode($data);

?>