<?php
/**
 * Created by PhpStorm.
 * User: keyuhan
 * Date: 2020/3/13
 * Time: 23:57
 */

    include('commonFunction.php');

    $cf = new commonFunction();


    $p_id = $cf->test_input($_POST['p_id']);

    $user_id = $cf->test_input($_POST['user_id']);

    // response data
    $data = array('code' => 'failure', 'msg' => '准备查询权限', 'data' => '');

    if (!empty($p_id)) {

        $p_authority = $cf->getValueByKey('p_authority', $con, $_PROJECTION_TABLE, 'p_id', $p_id);

        if($p_authority){

            if($p_authority == 'team'){

                if(strpos($user_id, 'tuku_') > -1){

                    $p_members = $cf->getValueByKey('p_members', $con, $_PROJECTION_TABLE, 'p_id', $p_id);

                    if(strpos($p_members,$user_id) > -1){

                        $p_authority = 'projection_member';// is projection member

                    } else {

                        $p_authority = 'not_projection_member';// is not projection member

                    }

                } else {

                    $p_authority = 'not_tuku_user';// is not tuku user

                }

            } else {

                // public or secret
            }

            $data['msg'] = '查询用户权限成功';

        } else {

            $p_authority = 'not_projection';

            $data['msg'] = '项目不存在';

        }

    } else {

        $p_authority = 'not_projection';

        $data['msg'] = '项目不存在';

    }

    $data['code'] = 'success';

    $data['data'] = array(

        'authority'=>$p_authority,// public , projection_member , not_projection_member , not_tuku_user, secret

    );

    mysqli_close($con);

    echo json_encode($data);

?>