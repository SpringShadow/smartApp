<?php
/**
 * Created by PhpStorm.
 * User: keke
 * Date: 2020/2/24
 * Time: 20:08
 */

    include('commonFunction.php');

    $cf = new commonFunction();


    $modelId = $cf->test_input($_POST["modelId"]);


    $data['code'] = 'failure';

    $data['msg']  = '准备获取文件的路径';

    $data['data']  = array();


    // get file_id , projection_id, and

    $sql = "SELECT * FROM  {$_MODEL_TABLE} WHERE m_id='$modelId'";

    $results = mysqli_query($con, $sql);
    
    if($results){

        $row = mysqli_fetch_array($results);

        $projectionPath =  $_SERVER['EDRAW_USERS_DIR'] . '/' . $row['m_creator_id'] . '/' . $row['m_projection'];

        $filePath = $projectionPath . '/' . $row['m_fileid'] . '.' . $row['m_suffix'];


        $data['code'] = 'success';

        $data['msg']  = '获取文件的路径成功';

        $data['data']  = array(

            'filePath'=>$filePath,

        );

    } else {

        $data['msg']  = '获取文件的路径失败';

    }

    mysqli_close($con);

    echo json_encode($data);

?>