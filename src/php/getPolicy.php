<?php
/**
 * Created by PhpStorm.
 * User: keyuhan
 * Date: 2020/1/25
 * Time: 19:35
 */

    include('commonFunction.php');

    $cf = new commonFunction();

    $Authorization = $cf->get_access_token();

    $fileName = $cf->test_input($_POST["fileName"]);


    $header = array(

        'Authorization: '.$Authorization,

        'Content-type: application/json'
    );

    $data['code'] = 'failure';

    $data['msg'] = '准备获取policy';

    $data['data'] = array();


    $policy = $cf->curl_http("https://file.bimface.com/upload/policy?name=".$fileName, "GET", $header, null);

    $policyArray = json_decode($policy, true);

    if($policyArray['code'] == 'success'){

        $data['code'] = 'success';

        $data['msg'] = '获取policy成功';

        $data['data'] = $policyArray['data'];

    }

    echo json_encode($data);

?>