import Vue from 'vue';

import App from './App.vue';

import vuetify from './plugins/vuetify';


import qs from 'qs'; //引入

Vue.prototype.qs = qs;  //全局加载,必须应用


import axios from 'axios';

Vue.prototype.axios = axios;

axios.defaults.baseURL = '/yun';

axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';

Vue.config.productionTip = false;


// 引入路由
import router from "./router/index.js";    // import router 的router 一定要小写， 不要写成Router, 否则报 can't match的报错

// 全局变量
import global_ from './components/Global.vue'

Vue.prototype.$G = global_;

// Vue Horizontal Timeline
import VueHorizontalTimeline from "vue-horizontal-timeline";

Vue.use(VueHorizontalTimeline);

// swiper
import VueAwesomeSwiper from 'vue-awesome-swiper';

import 'swiper/css/swiper.css';//这里注意具体看使用的版本是否需要引入样式，以及具体位置。

Vue.use(VueAwesomeSwiper);

// VueClipboard

import VueClipboard from 'vue-clipboard2'

VueClipboard.config.autoSetContainer = true // add this line

Vue.use(VueClipboard)


import VTooltip from 'v-tooltip'

Vue.use(VTooltip)



// v-viewer
import 'viewerjs/dist/viewer.css'

import Viewer from 'v-viewer'

Vue.use(Viewer, {

  defaultOptions: {

      zIndex: 2020,

      title: false,

      scalable: false,

      toolbar: {
          zoomIn: {show: 4, size: 'large'},
          zoomOut: {show: 4, size: 'large'},
          oneToOne: {show: 4, size: 'large'},
          reset: {show: 4, size: 'large'},
          prev: {show: 4, size: 'large'},
          play: 0,
          next: {show: 4, size: 'large'},
          rotateLeft: {show: 4, size: 'large'},
          rotateRight: {show: 4, size: 'large'},
          flipHorizontal: 0,
          flipVertical: 0,
      },

      loading: true,

  }

})



import './css/ass.css';

import './css/vuetify.css';


new Vue({
  vuetify,
  router,  // 注入到根实例中
  render: h => h(App)
}).$mount('#app')

