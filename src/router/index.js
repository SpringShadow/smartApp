import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import routes from './routes';

// import store from '../store/index.js';

const router = new Router({
	routes: routes,
	// mode: 'history',
	linkActiveClass: 'active-link',
	linkExactActiveClass: 'exact-active-link',
	scrollBehavior(to, from, savedPosition) {
		if (savedPosition) {
			return savedPosition;
		} else {
			return {
				x: 0,
				y: 0
			};
		}
	}
});
// 判断是否需要登录权限 以及是否登录
router.beforeEach((to, from, next) => {
	console.log(to);
	if (to.meta.requireAuth) { // 判断是否需要登录权限
		let token=localStorage.getItem("token")
		if (token) { // 判断是否登录
			next()
		} else { // 没登录则跳转到登录界面
			next({
				path: '/myLogin',
				// query: {
				// 	redirect: to.fullPath
				// }
			})
		}
	} else {
		next()
	}
})
export default router
