
// 引入组件
import home from "../views/home.vue";
import login from "../views/login.vue";
import today from "../views/appToday.vue";
import content from "../views/appContent.vue";
import weather from "../views/appWeather.vue";
import yinji from "../views/appYinji.vue";
import yinjirenwu from "../views/appJinjirenwu.vue";
import yinjirenwuinfo from "../views/appJinjirenwuinfo.vue";
import yinjiyuan from "../views/appJinjiyuan.vue";
import yinjiku from "../views/appJinjiku.vue";
import user from "../views/appUser.vue";
import event from "../views/appEvent.vue";

import message from "../views/message.vue";
import gis from "../views/gis.vue";
import communication from "../views/communication.vue";
import permission from "../views/permission.vue";
import setting from "../views/setting.vue";
import userInfo from "../views/userInfo.vue";
import myLogin from "../views/myLogin.vue";
import myRegister from "../views/myRegister.vue";
import inputTel from "../views/inputTel.vue";

import checkCode from "../views/checkCode.vue";
import setPassword from "../views/setPassword.vue";
import forget from "../views/forget.vue";


export default [
	{
		path: '/',
		redirect: '/myLogin'
	},
	{
	    path:"/home",
		meta:{
			requireAuth:true,
		},
		name:home,
		component: home,
	},
	
	{
	    path: "/login",
		meta:{
			requireAuth:false,
		},
		component:login,
	},
	{
	    path: "/myLogin",
		meta:{
			requireAuth:false,
		},
		component: myLogin,
	},
	{
	    path: "/inputTel",
		meta:{
			requireAuth:false,
		},
		component: inputTel,
	},
	{
	    path: "/myRegister",
		meta:{
			requireAuth:false,
		},
		component: myRegister,
	},
	{
	    path: "/checkCode",
		meta:{
			requireAuth:false,
		},
		component: checkCode,
	},
	{
	    path: "/setPassword",
		meta:{
			requireAuth:false,
		},
		component: setPassword,
	},
	{
	    path: "/forget",
		meta:{
			requireAuth:false,
		},
		component: forget,
	},
	{
	    path: "/user",
		meta:{
			requireAuth:true,
		},
		component: user,
	},
	{
	    path: "/gis",
		meta:{
			requireAuth:true,
		},
		component: gis,
	},
	{
	    path: "/today",
		meta:{
			requireAuth:true,
		},
		component: today,
	},
	{
	    path: "/content",
		meta:{
			requireAuth:true,
		},
		component: content,
	},
	{
	    path: "/weather",
		meta:{
			requireAuth:true,
		},
		component: weather,
	},
	{
	    path: "/yinji",
		meta:{
			requireAuth:true,
		},
		component: yinji,
	},
	{
	    path: "/yinjirenwu",
		meta:{
			requireAuth:true,
		},
		component: yinjirenwu,
	},
	{
	    path: "/yinjirenwuinfo",
		meta:{
			requireAuth:true,
		},
		component: yinjirenwuinfo,
	},
	{
	    path: "/yinjiyuan",
		meta:{
			requireAuth:true,
		},
		component: yinjiyuan,
	},
	{
	    path: "/yinjiku",
		meta:{
			requireAuth:true,
		},
		component: yinjiku,
	},
	{
	    path: "/event",
		meta:{
			requireAuth:true,
		},
		component: event,
	},
	{
	    path: "/message",
		meta:{
			requireAuth:true,
		},
		component: message,
	},
	{
	    path: "/communication",
		meta:{
			requireAuth:true,
		},
		component: communication,
	},
	{
	    path: "/permission",
		meta:{
			requireAuth:true,
		},
		component: permission,
	},
	{
	    path: "/setting",
		meta:{
			requireAuth:true,
		},
		component: setting,
	},
	{
	    path: "/userInfo",
		meta:{
			requireAuth:true,
		},
		component: userInfo,
	},
]

